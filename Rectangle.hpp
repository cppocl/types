/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPES_RECTANGLE_HPP
#define OCL_GUARD_TYPES_RECTANGLE_HPP

#include "internal/TypeTraits.hpp"
#include "internal/MinMax.hpp"
#include "Point.hpp"
#include "Size.hpp"

namespace ocl
{

template<typename Type>
class Rectangle
{
public:
    typedef Type type;
    typedef TypeTraits<type> type_traits;
    typedef typename type_traits::signed_type signed_type;
    typedef typename type_traits::unsigned_type unsigned_type;
    typedef Rectangle<signed_type> signed_rectangle_type;
    typedef Rectangle<unsigned_type> unsigned_rectangle_type;
    typedef Point<type> point_type;
    typedef Size<type> size_type;

public:
    Rectangle(type left = type_traits::Default(),
              type top = type_traits::Default(),
              type right = type_traits::Default(),
              type bottom = type_traits::Default()) noexcept
        : m_top_left(left, top)
        , m_bottom_right(right, bottom)
    {
    }

    Rectangle(point_type const& top_left, point_type const& bottom_right) noexcept
        : m_top_left(top_left)
        , m_bottom_right(bottom_right)
    {
    }

    Rectangle(Rectangle const& rectangle) noexcept
        : m_top_left(rectangle.m_top_left)
        , m_bottom_right(rectangle.m_bottom_right)
    {
    }

    Rectangle(Rectangle&& rectangle) noexcept
        : m_top_left(std::move(rectangle.m_top_left))
        , m_bottom_right(std::move(rectangle.m_bottom_right))
    {
    }

    bool operator ==(Rectangle<type> const& rectangle) const noexcept
    {
        return Left()   == rectangle.Left()  &&
               Top()    == rectangle.Top()   &&
               Right()  == rectangle.Right() &&
               Bottom() == rectangle.Bottom();
    }

    bool operator !=(Rectangle<type> const& rectangle) const noexcept
    {
        return Left()   != rectangle.Left()  ||
               Top()    != rectangle.Top()   ||
               Right()  != rectangle.Right() ||
               Bottom() != rectangle.Bottom();
    }

    Rectangle<type>& operator =(Rectangle<type> const& rectangle) noexcept
    {
        Copy(rectangle);
        return *this;
    }

    Rectangle<type>& operator =(Rectangle<type>&& rectangle) noexcept
    {
        Move(rectangle);
        return *this;
    }

    Rectangle<type>& operator +=(point_type const& point) noexcept
    {
        m_top_left += point;
        m_bottom_right += point;
        return *this;
    }

    Rectangle<type>& operator -=(point_type const& point) noexcept
    {
        m_top_left -= point;
        m_bottom_right -= point;
        return *this;
    }

    Rectangle<type>& operator *=(point_type const& point) noexcept
    {
        m_top_left *= point;
        m_bottom_right *= point;
        return *this;
    }

    Rectangle<type>& operator /=(point_type const& point) noexcept
    {
        m_top_left /= point;
        m_bottom_right /= point;
        return *this;
    }

    Rectangle<type> operator +(point_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_top_left += point;
        rect.m_bottom_right += point;
        return rect;
    }

    Rectangle<type> operator -(point_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_top_left -= point;
        rect.m_bottom_right -= point;
        return rect;
    }

    Rectangle<type> operator *(point_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_top_left *= point;
        rect.m_bottom_right *= point;
        return rect;
    }

    Rectangle<type> operator /(point_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_top_left /= point;
        rect.m_bottom_right /= point;
        return rect;
    }

    Rectangle<type>& operator +=(size_type const& point) noexcept
    {
        m_bottom_right += point;
        return *this;
    }

    Rectangle<type>& operator -=(size_type const& point) noexcept
    {
        m_bottom_right -= point;
        return *this;
    }

    Rectangle<type>& operator *=(size_type const& point) noexcept
    {
        m_bottom_right *= point;
        return *this;
    }

    Rectangle<type>& operator /=(size_type const& point) noexcept
    {
        m_bottom_right /= point;
        return *this;
    }

    Rectangle<type> operator +(size_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_bottom_right += point;
        return rect;
    }

    Rectangle<type> operator -(size_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_bottom_right -= point;
        return rect;
    }

    Rectangle<type> operator *(size_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_bottom_right *= point;
        return rect;
    }

    Rectangle<type> operator /(size_type const& point) const noexcept
    {
        Rectangle<type> rect(*this);
        rect.m_bottom_right /= point;
        return rect;
    }

    type Left() const noexcept
    {
        return m_top_left.X();
    }

    type Top() const noexcept
    {
        return m_top_left.Y();
    }

    type Right() const noexcept
    {
        return m_bottom_right.X();
    }

    type Bottom() const noexcept
    {
        return m_bottom_right.Y();
    }

    point_type TopLeft() const noexcept
    {
        return m_top_left;
    }

    point_type BottomRight() const noexcept
    {
        return m_bottom_right;
    }

    type Width() const noexcept
    {
        return type_traits::One() + Right() - Left();
    }

    type Height() const noexcept
    {
        return type_traits::One() + Bottom() - Top();
    }

    void Copy(Rectangle<type> const& rectangle) noexcept
    {
        m_top_left.Copy(rectangle.m_top_left);
        m_bottom_right.Copy(rectangle.m_bottom_right);
    }

    void Move(Rectangle<type>& rectangle) noexcept
    {
        m_top_left.Move(rectangle.m_top_left);
        m_bottom_right.Move(rectangle.m_bottom_right);
    }

    // Get the size of the rectangle.
    // e.g. Rectangle(1,1, 2,2) will return Size(2, 2)
    size_type GetSize() const noexcept
    {
        type width  = m_top_left.X() < m_bottom_right.X() ? type_traits::One() + m_bottom_right.X() - m_top_left.X()
                                                          : type_traits::One() + m_top_left.X() - m_bottom_right.X();
        type height = m_top_left.Y() < m_bottom_right.Y() ? type_traits::One() + m_bottom_right.Y() - m_top_left.Y()
                                                          : type_traits::One() + m_top_left.Y() - m_bottom_right.Y();
        return size_type(width, height);
    }

    // See if the two rectangles intersect.
    bool IsIntersection(Rectangle<type> const& other) const noexcept
    {
        return !((other.Right()  < Left()) ||
                 (other.Left()   > Right()) ||
                 (other.Bottom() < Top()) ||
                 (other.Top()    > Bottom()));
    }

    // Get the smaller overlapping region.
    Rectangle<type> GetIntersection(Rectangle<type> const& other) const noexcept
    {
        typedef ocl::MinMax<type> min_max;

        Rectangle<type> intersection(min_max::Max(Left(), other.Left()),
                                     min_max::Max(Top(), other.Top()),
                                     min_max::Min(Right(), other.Right()),
                                     min_max::Min(Bottom(), other.Bottom()));

        return intersection;
    }

    // Get a rectangle that encompasses both rectangles.
    Rectangle<type> GetPerimeter(Rectangle<type> const& other) const noexcept
    {
        typedef ocl::MinMax<type> min_max;

        Rectangle<type> perimeter(min_max::Min(Left(), other.Left()),
                                  min_max::Min(Top(), other.Top()),
                                  min_max::Max(Right(), other.Right()),
                                  min_max::Max(Bottom(), other.Bottom()));

        return perimeter;
    }

    void Swap(Rectangle<type>& other) noexcept
    {
        m_top_left.Swap(other.m_top_left);
        m_bottom_right.Swap(other.m_bottom_right);
    }

    // Get the type as a string.
    static constexpr char const* TypeString() noexcept
    {
        return type_traits::TypeString();
    }

private:
    point_type m_top_left;
    point_type m_bottom_right;
};

} // namespace ocl

#endif // OCL_GUARD_TYPES_RECTANGLE_HPP
