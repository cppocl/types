/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../TriState.hpp"
#include <utility>

TEST_MEMBER_FUNCTION(TriState, TriState, NA)
{
    ocl::TriState state;
    CHECK_TRUE(state.IsUnknown());
    CHECK_FALSE(state.IsKnown());
}

TEST_MEMBER_FUNCTION(TriState, TriState, bool)
{
    {
        ocl::TriState state(true);
        CHECK_TRUE(state.IsTrue());
        CHECK_TRUE(state.IsKnown());
    }

    {
        ocl::TriState state(false);
        CHECK_TRUE(state.IsFalse());
        CHECK_TRUE(state.IsKnown());
    }
}

TEST_MEMBER_FUNCTION(TriState, TriState, TriState)
{
    {
        ocl::TriState state1;
        ocl::TriState state2(state1);
        CHECK_TRUE(state2.IsUnknown());
    }

    {
        ocl::TriState state1(true);
        ocl::TriState state2(state1);
        CHECK_TRUE(state2.IsTrue());
    }

    {
        ocl::TriState state1(false);
        ocl::TriState state2(state1);
        CHECK_TRUE(state2.IsFalse());
    }
}

TEST_MEMBER_FUNCTION(TriState, TriState, TriState_move)
{
    TEST_OVERRIDE_ARGS("TriState&&");

    {
        ocl::TriState state1;
        ocl::TriState state2(std::move(state1));
        CHECK_TRUE(state1.IsUnknown());
        CHECK_TRUE(state2.IsUnknown());
    }

    {
        ocl::TriState state1(true);
        ocl::TriState state2(std::move(state1));
        CHECK_TRUE(state1.IsUnknown());
        CHECK_TRUE(state2.IsTrue());
    }

    {
        ocl::TriState state1(false);
        ocl::TriState state2(std::move(state1));
        CHECK_TRUE(state1.IsUnknown());
        CHECK_TRUE(state2.IsFalse());
    }
}

TEST_MEMBER_FUNCTION(TriState, operator_eq, TriState)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator =", "TriState const&");

    {
        ocl::TriState state1;
        ocl::TriState state2;
        state2 = state1;
        CHECK_TRUE(state1.IsUnknown());
        CHECK_TRUE(state2.IsUnknown());
    }

    {
        ocl::TriState state1(true);
        ocl::TriState state2;
        state2 = state1;
        CHECK_TRUE(state1.IsTrue());
        CHECK_TRUE(state2.IsTrue());
    }

    {
        ocl::TriState state1(false);
        ocl::TriState state2;
        state2 = state1;
        CHECK_TRUE(state1.IsFalse());
        CHECK_TRUE(state2.IsFalse());
    }
}

TEST_MEMBER_FUNCTION(TriState, operator_eq, TriState_move)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator =", "TriState&&");

    {
        ocl::TriState state1;
        ocl::TriState state2;
        state2 = std::move(state1);
        CHECK_TRUE(state1.IsUnknown());
        CHECK_TRUE(state2.IsUnknown());
    }

    {
        ocl::TriState state1(true);
        ocl::TriState state2;
        state2 = std::move(state1);
        CHECK_TRUE(state1.IsUnknown());
        CHECK_TRUE(state2.IsTrue());
    }

    {
        ocl::TriState state1(false);
        ocl::TriState state2;
        state2 = std::move(state1);
        CHECK_TRUE(state1.IsUnknown());
        CHECK_TRUE(state2.IsFalse());
    }
}

TEST_MEMBER_FUNCTION(TriState, operator_eq, bool)
{
    ocl::TriState state;

    state = true;
    CHECK_TRUE(state.IsTrue());

    state = false;
    CHECK_TRUE(state.IsFalse());
}

TEST_MEMBER_FUNCTION(TriState, operator_bool, NA)
{
    {
        bool is_true = false;
        bool caught_exception = false;

        ocl::TriState state;
        try
        {
            is_true = static_cast<bool>(state);
        }
        catch (ocl::TriState::MessageException&)
        {
            caught_exception = true;
        }
        CHECK_TRUE(caught_exception);
    }

    {
        ocl::TriState state(true);
        bool is_true = static_cast<bool>(state);
        CHECK_TRUE(is_true);
    }

    {
        ocl::TriState state(false);
        bool is_true = static_cast<bool>(state);
        CHECK_FALSE(is_true);
    }
}

TEST_MEMBER_FUNCTION(TriState, Clear, NA)
{
    ocl::TriState state;

    state.Clear();
    CHECK_TRUE(state.IsUnknown());

    state = true;
    state.Clear();
    CHECK_TRUE(state.IsUnknown());

    state = false;
    state.Clear();
    CHECK_TRUE(state.IsUnknown());
}
