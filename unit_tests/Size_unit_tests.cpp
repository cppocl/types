/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../Size.hpp"
#include <utility>

TEST_MEMBER_FUNCTION(Size, Size, NA)
{
    ocl::Size<int> sz;

    CHECK_TRUE(sz.Width() == ocl::TypeTraits<int>::Default());
    CHECK_TRUE(sz.Height() == ocl::TypeTraits<int>::Default());
}

TEST_MEMBER_FUNCTION(Size, Size, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Size<int> sz(1, 2);

    CHECK_TRUE(sz.Width() == 1);
    CHECK_TRUE(sz.Height() == 2);
}

TEST_MEMBER_FUNCTION(Size, Size, Size_const_ref)
{
    TEST_OVERRIDE_ARGS("Size const&");

    typedef ocl::Size<int> size_type;

    size_type sz1(1, 2);
    size_type sz2(sz1);

    CHECK_TRUE(sz2.Width() == 1);
    CHECK_TRUE(sz2.Height() == 2);
}

TEST_MEMBER_FUNCTION(Size, Size, Size_move)
{
    TEST_OVERRIDE_ARGS("Size&&");

    typedef ocl::Size<int> size_type;

    size_type sz1(1, 2);
    size_type sz2(std::move(sz1));

    CHECK_TRUE(sz2.Width() == 1);
    CHECK_TRUE(sz2.Height() == 2);
}

TEST_MEMBER_FUNCTION(Size, operator_is_equal, Size)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator ==");

    typedef ocl::Size<int> size_type;

    size_type sz;
    sz = size_type(1, 2);

    CHECK_TRUE(sz == size_type(1, 2));
    CHECK_FALSE(sz == size_type(2, 2));
    CHECK_FALSE(sz == size_type(1, 1));
    CHECK_FALSE(sz == size_type(3, 3));
}

TEST_MEMBER_FUNCTION(Size, operator_is_not_equal, Size)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator !=");

    typedef ocl::Size<int> size_type;

    size_type sz;
    sz = size_type(1, 2);

    CHECK_FALSE(sz != size_type(1, 2));
    CHECK_TRUE(sz != size_type(2, 2));
    CHECK_TRUE(sz != size_type(1, 1));
    CHECK_TRUE(sz != size_type(3, 3));
}

TEST_MEMBER_FUNCTION(Size, operator_eq, Size)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator =");

    typedef ocl::Size<int> size_type;

    size_type sz;
    sz = size_type(1, 2);

    CHECK_TRUE(sz.Width() == 1);
    CHECK_TRUE(sz.Height() == 2);
}

TEST_MEMBER_FUNCTION(Size, operator_plus_eq, Size)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator +=");

    typedef ocl::Size<int> size_type;

    size_type sz(1, 2);
    sz += size_type(2, 4);

    CHECK_TRUE(sz.Width() == 3);
    CHECK_TRUE(sz.Height() == 6);
}

TEST_MEMBER_FUNCTION(Size, operator_minus_eq, Size)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator -=");

    typedef ocl::Size<int> size_type;

    size_type sz(3, 6);
    sz -= size_type(2, 4);

    CHECK_TRUE(sz.Width() == 1);
    CHECK_TRUE(sz.Height() == 2);
}

TEST_MEMBER_FUNCTION(Size, operator_multiply_eq, Size)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator *=");

    typedef ocl::Size<int> size_type;

    size_type sz(2, 3);
    sz *= size_type(2, 3);

    CHECK_TRUE(sz.Width() == 4);
    CHECK_TRUE(sz.Height() == 9);
}

TEST_MEMBER_FUNCTION(Size, operator_divide_eq, Size)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator /=");

    typedef ocl::Size<int> size_type;

    size_type sz(4, 9);
    sz /= size_type(2, 3);

    CHECK_TRUE(sz.Width() == 2);
    CHECK_TRUE(sz.Height() == 3);
}

TEST_MEMBER_FUNCTION(Size, operator_plus, Size_Size)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator +", "Size, Size");

    typedef ocl::Size<int> size_type;

    size_type sz1(1, 2);
    size_type sz2(2, 4);
    size_type sz = sz1 + sz2;

    CHECK_TRUE(sz.Width() == 3);
    CHECK_TRUE(sz.Height() == 6);
}

TEST_MEMBER_FUNCTION(Size, operator_minus, Size_Size)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator -", "Size, Size");

    typedef ocl::Size<int> size_type;

    size_type sz1(4, 8);
    size_type sz2(2, 4);
    size_type sz = sz1 - sz2;

    CHECK_TRUE(sz.Width() == 2);
    CHECK_TRUE(sz.Height() == 4);
}

TEST_MEMBER_FUNCTION(Size, operator_multiply, Size_Size)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator *", "Size, Size");

    typedef ocl::Size<int> size_type;

    size_type sz1(1, 2);
    size_type sz2(2, 3);
    size_type sz = sz1 * sz2;

    CHECK_TRUE(sz.Width() == 2);
    CHECK_TRUE(sz.Height() == 6);
}

TEST_MEMBER_FUNCTION(Size, operator_divide, Size_Size)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator /", "Size, Size");

    typedef ocl::Size<int> size_type;

    size_type sz1(4, 8);
    size_type sz2(2, 4);
    size_type sz = sz1 - sz2;

    CHECK_TRUE(sz.Width() == 2);
    CHECK_TRUE(sz.Height() == 4);
}

TEST_MEMBER_FUNCTION(Size, Copy, Size)
{
    typedef ocl::Size<int> size_type;

    size_type sz1;
    size_type sz2(2, 4);

    sz1.Copy(sz2);
    CHECK_TRUE(sz1.Width() == 2);
    CHECK_TRUE(sz1.Height() == 4);
}

TEST_MEMBER_FUNCTION(Size, Move, Size)
{
    typedef ocl::Size<int> size_type;

    size_type sz1;
    size_type sz2(2, 4);

    sz1.Move(sz2);
    CHECK_TRUE(sz1.Width() == 2);
    CHECK_TRUE(sz1.Height() == 4);
}

TEST_MEMBER_FUNCTION(Size, IncrementWidth, int)
{
    ocl::Size<int> sz(2, 4);

    sz.IncrementWidth(1);
    CHECK_TRUE(sz.Width() == 3);

    sz.IncrementWidth(2);
    CHECK_TRUE(sz.Width() == 5);
}

TEST_MEMBER_FUNCTION(Size, IncrementHeight, int)
{
    ocl::Size<int> sz(2, 4);

    sz.IncrementHeight(1);
    CHECK_TRUE(sz.Height() == 5);

    sz.IncrementHeight(2);
    CHECK_TRUE(sz.Height() == 7);
}

TEST_MEMBER_FUNCTION(Size, IncrementWidthHeight, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Size<int> sz(2, 4);

    sz.IncrementWidth(1);
    sz.IncrementHeight(2);
    CHECK_TRUE(sz.Width() == 3);
    CHECK_TRUE(sz.Height() == 6);

    sz.IncrementWidth(2);
    sz.IncrementHeight(1);
    CHECK_TRUE(sz.Width() == 5);
    CHECK_TRUE(sz.Height() == 7);
}

TEST_MEMBER_FUNCTION(Size, DecrementWidth, int)
{
    ocl::Size<int> sz(4, 6);

    sz.DecrementWidth(1);
    CHECK_TRUE(sz.Width() == 3);

    sz.DecrementWidth(2);
    CHECK_TRUE(sz.Width() == 1);
}

TEST_MEMBER_FUNCTION(Size, DecrementHeight, int)
{
    ocl::Size<int> sz(4, 6);

    sz.DecrementHeight(1);
    CHECK_TRUE(sz.Height() == 5);

    sz.DecrementHeight(2);
    CHECK_TRUE(sz.Height() == 3);
}

TEST_MEMBER_FUNCTION(Size, DecrementWidthHeight, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Size<int> sz(4, 6);

    sz.DecrementWidthHeight(1, 2);
    CHECK_TRUE(sz.Width() == 3);
    CHECK_TRUE(sz.Height() == 4);

    sz.DecrementWidthHeight(2, 1);
    CHECK_TRUE(sz.Width() == 1);
    CHECK_TRUE(sz.Height() == 3);
}

TEST_MEMBER_FUNCTION(Size, SetWidth, int)
{
    ocl::Size<int> sz;

    sz.SetWidth(1);
    CHECK_TRUE(sz.Width() == 1);

    sz.SetWidth(2);
    CHECK_TRUE(sz.Width() == 2);
}

TEST_MEMBER_FUNCTION(Size, SetHeight, int)
{
    ocl::Size<int> sz;

    sz.SetHeight(1);
    CHECK_TRUE(sz.Height() == 1);

    sz.SetHeight(2);
    CHECK_TRUE(sz.Height() == 2);
}

TEST_MEMBER_FUNCTION(Size, SetWidthHeight, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Size<int> sz;

    sz.SetWidthHeight(1, 2);
    CHECK_TRUE(sz.Width() == 1);
    CHECK_TRUE(sz.Height() == 2);

    sz.SetWidthHeight(2, 1);
    CHECK_TRUE(sz.Width() == 2);
    CHECK_TRUE(sz.Height() == 1);
}

TEST_MEMBER_FUNCTION(Size, OffsetWidth, int)
{
    ocl::Size<int> sz;

    sz.OffsetWidth(1);
    CHECK_TRUE(sz.Width() == 1);

    sz.OffsetWidth(2);
    CHECK_TRUE(sz.Width() == 3);

    sz.OffsetWidth(-1);
    CHECK_TRUE(sz.Width() == 2);

    sz.OffsetWidth(-3);
    CHECK_TRUE(sz.Width() == -1);
}

TEST_MEMBER_FUNCTION(Size, OffsetHeight, int)
{
    ocl::Size<int> sz;

    sz.OffsetHeight(1);
    CHECK_TRUE(sz.Height() == 1);

    sz.OffsetHeight(2);
    CHECK_TRUE(sz.Height() == 3);

    sz.OffsetHeight(-1);
    CHECK_TRUE(sz.Height() == 2);

    sz.OffsetHeight(-3);
    CHECK_TRUE(sz.Height() == -1);
}

TEST_MEMBER_FUNCTION(Size, OffsetWidthHeight, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Size<int> sz;

    sz.OffsetWidthHeight(1, 2);
    CHECK_TRUE(sz.Width() == 1);
    CHECK_TRUE(sz.Height() == 2);

    sz.OffsetWidthHeight(2, 3);
    CHECK_TRUE(sz.Width() == 3);
    CHECK_TRUE(sz.Height() == 5);

    sz.OffsetWidthHeight(-1, -2);
    CHECK_TRUE(sz.Width() == 2);
    CHECK_TRUE(sz.Height() == 3);

    sz.OffsetWidthHeight(-3, -5);
    CHECK_TRUE(sz.Width() == -1);
    CHECK_TRUE(sz.Height() == -2);
}

TEST_MEMBER_FUNCTION(Size, Clear, NA)
{
    ocl::Size<int> sz(1, 2);

    sz.Clear();
    CHECK_TRUE(sz.Width() == 0);
    CHECK_TRUE(sz.Height() == 0);
}

TEST_MEMBER_FUNCTION(Size, BoundsCheckWidth, int)
{
    {
        ocl::Size<signed char> sz(126, 0);

        CHECK_TRUE(sz.BoundsCheckWidth(1, true));
        CHECK_FALSE(sz.BoundsCheckWidth(2, true));
        CHECK_TRUE(sz.BoundsCheckWidth(-1, false));
        CHECK_FALSE(sz.BoundsCheckWidth(-2, false));
    }

    {
        ocl::Size<signed char> sz(-127, 0);

        CHECK_TRUE(sz.BoundsCheckWidth(-1, true));
        CHECK_FALSE(sz.BoundsCheckWidth(-2, true));
        CHECK_TRUE(sz.BoundsCheckWidth(1, false));
        CHECK_FALSE(sz.BoundsCheckWidth(2, false));
    }
}

TEST_MEMBER_FUNCTION(Size, BoundsCheckWidth, unsigned_int)
{
    TEST_OVERRIDE_ARGS("unsigned int");

    {
        ocl::Size<unsigned char> sz(254U, 0U);

        CHECK_TRUE(sz.BoundsCheckWidth(1, true));
        CHECK_FALSE(sz.BoundsCheckWidth(2, true));
    }

    {
        ocl::Size<unsigned char> sz(1U, 0U);

        CHECK_TRUE(sz.BoundsCheckWidth(1, false));
        CHECK_FALSE(sz.BoundsCheckWidth(2, false));
    }
}

TEST_MEMBER_FUNCTION(Size, BoundsCheckHeight, int)
{
    {
        ocl::Size<signed char> sz(0, 126);

        CHECK_TRUE(sz.BoundsCheckHeight(1, true));
        CHECK_FALSE(sz.BoundsCheckHeight(2, true));
        CHECK_TRUE(sz.BoundsCheckHeight(-1, false));
        CHECK_FALSE(sz.BoundsCheckHeight(-2, false));
    }

    {
        ocl::Size<signed char> sz(0, -127);

        CHECK_TRUE(sz.BoundsCheckHeight(-1, true));
        CHECK_FALSE(sz.BoundsCheckHeight(-2, true));
        CHECK_TRUE(sz.BoundsCheckHeight(1, false));
        CHECK_FALSE(sz.BoundsCheckHeight(2, false));
    }
}

TEST_MEMBER_FUNCTION(Size, BoundsCheckHeight, unsigned_int)
{
    TEST_OVERRIDE_ARGS("unsigned int");

    {
        ocl::Size<unsigned char> sz(0U, 254U);

        CHECK_TRUE(sz.BoundsCheckHeight(1, true));
        CHECK_FALSE(sz.BoundsCheckHeight(2, true));
    }

    {
        ocl::Size<unsigned char> sz(0U, 1U);

        CHECK_TRUE(sz.BoundsCheckHeight(1, false));
        CHECK_FALSE(sz.BoundsCheckHeight(2, false));
    }
}

TEST_MEMBER_FUNCTION(Size, BoundsCheckWidthHeight, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    {
        ocl::Size<signed char> sz(126, 0);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 0, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 0, true));
        CHECK_TRUE(sz.BoundsCheckWidthHeight(-1, 0, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(-2, 0, false));
    }

    {
        ocl::Size<signed char> sz(0, 126);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(0, 1, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(0, 2, true));
        CHECK_TRUE(sz.BoundsCheckWidthHeight(0, -1, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(0, -2, false));
    }

    {
        ocl::Size<signed char> sz(126, 126);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 1, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 2, true));
        CHECK_TRUE(sz.BoundsCheckWidthHeight(-1, -1, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(-2, -2, false));
    }

    {
        ocl::Size<signed char> sz(-127, 0);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(-1, 0, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(-2, 0, true));
        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 0, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 0, false));
    }

    {
        ocl::Size<signed char> sz(0, -127);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(0, -1, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(0, -2, true));
        CHECK_TRUE(sz.BoundsCheckWidthHeight(0, 1, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(0, 2, false));
    }

    {
        ocl::Size<signed char> sz(-127, -127);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(-1, -1, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(-2, -2, true));
        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 1, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 2, false));
    }
}

TEST_MEMBER_FUNCTION(Size, BoundsCheckWidthHeight, unsigned_int_unsigned_int)
{
    TEST_OVERRIDE_ARGS("unsigned int, unsigned int");

    {
        ocl::Size<unsigned char> sz(254U, 0U);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 0, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 0, true));
    }

    {
        ocl::Size<unsigned char> sz(0U, 254U);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(0, 1, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(0, 2, true));
    }

    {
        ocl::Size<unsigned char> sz(254U, 254U);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 1, true));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 2, true));
    }

    {
        ocl::Size<unsigned char> sz(1U, 0U);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 0, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 0, false));
    }

    {
        ocl::Size<unsigned char> sz(0U, 1U);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(0, 1, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(0, 2, false));
    }

    {
        ocl::Size<unsigned char> sz(1U, 1U);

        CHECK_TRUE(sz.BoundsCheckWidthHeight(1, 1, false));
        CHECK_FALSE(sz.BoundsCheckWidthHeight(2, 2, false));
    }
}

TEST_MEMBER_FUNCTION(Size, SignedCast, Size)
{
    ocl::Size<unsigned int> sz1(1U, 2U);
    ocl::Size<int> sz2(sz1.SignedCast());
    CHECK_TRUE(sz2.Width() == 1);
    CHECK_TRUE(sz2.Height() == 2);
}

TEST_MEMBER_FUNCTION(Size, UnsignedCast, Size)
{
    ocl::Size<int> sz1(1, 2);
    ocl::Size<unsigned int> sz2(sz1.UnsignedCast());
    CHECK_TRUE(sz2.Width() == 1U);
    CHECK_TRUE(sz2.Height() == 2U);
}

TEST_MEMBER_FUNCTION(Size, GetDiff, Size)
{
    {
        typedef ocl::Size<int> size_type;

        size_type sz1(1, 2);
        size_type sz2(3, 5);

        size_type diff = sz1.GetDiff(sz2);
        CHECK_TRUE(diff.Width() == -2);
        CHECK_TRUE(diff.Height() == -3);

        diff = sz2.GetDiff(sz1);
        CHECK_TRUE(diff.Width() == 2);
        CHECK_TRUE(diff.Height() == 3);
    }

    {
        typedef ocl::Size<unsigned int> size_type;

        size_type sz1(1, 2);
        size_type sz2(3, 5);

        ocl::Size<int> diff = sz1.GetDiff(sz2);
        CHECK_TRUE(diff.Width() == -2);
        CHECK_TRUE(diff.Height() == -3);

        diff = sz2.GetDiff(sz1);
        CHECK_TRUE(diff.Width() == 2);
        CHECK_TRUE(diff.Height() == 3);
    }
}

TEST_MEMBER_FUNCTION(Size, GetAbsDiff, Size)
{
    {
        typedef ocl::Size<int> size_type;

        size_type sz1(1, 2);
        size_type sz2(3, 5);

        size_type diff = sz1.GetAbsDiff(sz2);
        CHECK_TRUE(diff.Width() == 2);
        CHECK_TRUE(diff.Height() == 3);

        diff = sz2.GetAbsDiff(sz1);
        CHECK_TRUE(diff.Width() == 2);
        CHECK_TRUE(diff.Height() == 3);
    }

    {
        typedef ocl::Size<unsigned int> size_type;

        size_type sz1(1, 2);
        size_type sz2(3, 5);

        size_type diff = sz1.GetAbsDiff(sz2);
        CHECK_TRUE(diff.Width() == 2);
        CHECK_TRUE(diff.Height() == 3);

        diff = sz2.GetAbsDiff(sz1);
        CHECK_TRUE(diff.Width() == 2);
        CHECK_TRUE(diff.Height() == 3);
    }
}

TEST_MEMBER_FUNCTION(Size, Swap, Size_ref)
{
    TEST_OVERRIDE_ARGS("Size&");

    typedef ocl::Size<int> size_type;

    size_type sz1(1, 2);
    size_type sz2(3, 4);

    sz1.Swap(sz2);

    CHECK_TRUE(sz1.Width() == 3);
    CHECK_TRUE(sz1.Height() == 4);
    CHECK_TRUE(sz2.Width() == 1);
    CHECK_TRUE(sz2.Height() == 2);
}
