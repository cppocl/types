/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../Point.hpp"
#include <utility>

TEST_MEMBER_FUNCTION(Point, Point, NA)
{
    ocl::Point<int> p;

    CHECK_TRUE(p.X() == ocl::TypeTraits<int>::Default());
    CHECK_TRUE(p.Y() == ocl::TypeTraits<int>::Default());
}

TEST_MEMBER_FUNCTION(Point, Point, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Point<int> p(1, 2);

    CHECK_TRUE(p.X() == 1);
    CHECK_TRUE(p.Y() == 2);
}

TEST_MEMBER_FUNCTION(Point, Point, Point_const_ref)
{
    TEST_OVERRIDE_ARGS("Point const&");

    typedef ocl::Point<int> point_type;

    point_type p1(1, 2);
    point_type p2(p1);

    CHECK_TRUE(p2.X() == 1);
    CHECK_TRUE(p2.Y() == 2);
}

TEST_MEMBER_FUNCTION(Point, Point, Point_move)
{
    TEST_OVERRIDE_ARGS("Point&&");

    typedef ocl::Point<int> point_type;

    point_type p1(1, 2);
    point_type p2(std::move(p1));

    CHECK_TRUE(p2.X() == 1);
    CHECK_TRUE(p2.Y() == 2);
}

TEST_MEMBER_FUNCTION(Point, operator_is_equal, Point)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator ==");

    typedef ocl::Point<int> point_type;

    point_type p;
    p = point_type(1, 2);

    CHECK_TRUE(p == point_type(1, 2));
    CHECK_FALSE(p == point_type(2, 2));
    CHECK_FALSE(p == point_type(1, 1));
    CHECK_FALSE(p == point_type(3, 3));
}

TEST_MEMBER_FUNCTION(Point, operator_is_not_equal, Point)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator !=");

    typedef ocl::Point<int> point_type;

    point_type p;
    p = point_type(1, 2);

    CHECK_FALSE(p != point_type(1, 2));
    CHECK_TRUE(p != point_type(2, 2));
    CHECK_TRUE(p != point_type(1, 1));
    CHECK_TRUE(p != point_type(3, 3));
}

TEST_MEMBER_FUNCTION(Point, operator_eq, Point)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator =");

    typedef ocl::Point<int> point_type;

    point_type p;
    p = point_type(1, 2);

    CHECK_TRUE(p.X() == 1);
    CHECK_TRUE(p.Y() == 2);
}

TEST_MEMBER_FUNCTION(Point, operator_plus_eq, Point)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator +=");

    typedef ocl::Point<int> point_type;

    point_type p(1, 2);
    p += point_type(2, 4);

    CHECK_TRUE(p.X() == 3);
    CHECK_TRUE(p.Y() == 6);
}

TEST_MEMBER_FUNCTION(Point, operator_minus_eq, Point)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator -=");

    typedef ocl::Point<int> point_type;

    point_type p(3, 6);
    p -= point_type(2, 4);

    CHECK_TRUE(p.X() == 1);
    CHECK_TRUE(p.Y() == 2);
}

TEST_MEMBER_FUNCTION(Point, operator_multiply_eq, Point)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator *=");

    typedef ocl::Point<int> point_type;

    point_type p(2, 3);
    p *= point_type(2, 3);

    CHECK_TRUE(p.X() == 4);
    CHECK_TRUE(p.Y() == 9);
}

TEST_MEMBER_FUNCTION(Point, operator_divide_eq, Point)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator /=");

    typedef ocl::Point<int> point_type;

    point_type p(4, 9);
    p /= point_type(2, 3);

    CHECK_TRUE(p.X() == 2);
    CHECK_TRUE(p.Y() == 3);
}

TEST_MEMBER_FUNCTION(Point, operator_plus, Point_Point)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator +", "Point, Point");

    typedef ocl::Point<int> point_type;

    point_type p1(1, 2);
    point_type p2(2, 4);
    point_type p = p1 + p2;

    CHECK_TRUE(p.X() == 3);
    CHECK_TRUE(p.Y() == 6);
}

TEST_MEMBER_FUNCTION(Point, operator_minus, Point_Point)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator -", "Point, Point");

    typedef ocl::Point<int> point_type;

    point_type p1(4, 8);
    point_type p2(2, 4);
    point_type p = p1 - p2;

    CHECK_TRUE(p.X() == 2);
    CHECK_TRUE(p.Y() == 4);
}

TEST_MEMBER_FUNCTION(Point, operator_multiply, Point_Point)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator *", "Point, Point");

    typedef ocl::Point<int> point_type;

    point_type p1(1, 2);
    point_type p2(2, 3);
    point_type p = p1 * p2;

    CHECK_TRUE(p.X() == 2);
    CHECK_TRUE(p.Y() == 6);
}

TEST_MEMBER_FUNCTION(Point, operator_divide, Point_Point)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator /", "Point, Point");

    typedef ocl::Point<int> point_type;

    point_type p1(4, 8);
    point_type p2(2, 4);
    point_type p = p1 - p2;

    CHECK_TRUE(p.X() == 2);
    CHECK_TRUE(p.Y() == 4);
}

TEST_MEMBER_FUNCTION(Point, Copy, Point)
{
    typedef ocl::Point<int> point_type;

    point_type p1;
    point_type p2(2, 4);

    p1.Copy(p2);
    CHECK_TRUE(p1.X() == 2);
    CHECK_TRUE(p1.Y() == 4);
}

TEST_MEMBER_FUNCTION(Point, Move, Point)
{
    typedef ocl::Point<int> point_type;

    point_type p1;
    point_type p2(2, 4);

    p1.Move(p2);
    CHECK_TRUE(p1.X() == 2);
    CHECK_TRUE(p1.Y() == 4);
}

TEST_MEMBER_FUNCTION(Point, IncrementX, int)
{
    ocl::Point<int> p(2, 4);

    p.IncrementX(1);
    CHECK_TRUE(p.X() == 3);

    p.IncrementX(2);
    CHECK_TRUE(p.X() == 5);
}

TEST_MEMBER_FUNCTION(Point, IncrementY, int)
{
    ocl::Point<int> p(2, 4);

    p.IncrementY(1);
    CHECK_TRUE(p.Y() == 5);

    p.IncrementY(2);
    CHECK_TRUE(p.Y() == 7);
}

TEST_MEMBER_FUNCTION(Point, IncrementXY, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Point<int> p(2, 4);

    p.IncrementXY(1, 2);
    CHECK_TRUE(p.X() == 3);
    CHECK_TRUE(p.Y() == 6);

    p.IncrementXY(2, 1);
    CHECK_TRUE(p.X() == 5);
    CHECK_TRUE(p.Y() == 7);
}

TEST_MEMBER_FUNCTION(Point, DecrementX, int)
{
    ocl::Point<int> p(4, 6);

    p.DecrementX(1);
    CHECK_TRUE(p.X() == 3);

    p.DecrementX(2);
    CHECK_TRUE(p.X() == 1);
}

TEST_MEMBER_FUNCTION(Point, DecrementY, int)
{
    ocl::Point<int> p(4, 6);

    p.DecrementY(1);
    CHECK_TRUE(p.Y() == 5);

    p.DecrementY(2);
    CHECK_TRUE(p.Y() == 3);
}

TEST_MEMBER_FUNCTION(Point, DecrementXY, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Point<int> p(4, 6);

    p.DecrementXY(1, 2);
    CHECK_TRUE(p.X() == 3);
    CHECK_TRUE(p.Y() == 4);

    p.DecrementXY(2, 1);
    CHECK_TRUE(p.X() == 1);
    CHECK_TRUE(p.Y() == 3);
}

TEST_MEMBER_FUNCTION(Point, SetX, int)
{
    ocl::Point<int> p;

    p.SetX(1);
    CHECK_TRUE(p.X() == 1);

    p.SetX(2);
    CHECK_TRUE(p.X() == 2);
}

TEST_MEMBER_FUNCTION(Point, SetY, int)
{
    ocl::Point<int> p;

    p.SetY(1);
    CHECK_TRUE(p.Y() == 1);

    p.SetY(2);
    CHECK_TRUE(p.Y() == 2);
}

TEST_MEMBER_FUNCTION(Point, SetXY, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Point<int> p;

    p.SetXY(1, 2);
    CHECK_TRUE(p.X() == 1);
    CHECK_TRUE(p.Y() == 2);

    p.SetXY(2, 1);
    CHECK_TRUE(p.X() == 2);
    CHECK_TRUE(p.Y() == 1);
}

TEST_MEMBER_FUNCTION(Point, OffsetX, int)
{
    ocl::Point<int> p;

    p.OffsetX(1);
    CHECK_TRUE(p.X() == 1);

    p.OffsetX(2);
    CHECK_TRUE(p.X() == 3);

    p.OffsetX(-1);
    CHECK_TRUE(p.X() == 2);

    p.OffsetX(-3);
    CHECK_TRUE(p.X() == -1);
}

TEST_MEMBER_FUNCTION(Point, OffsetY, int)
{
    ocl::Point<int> p;

    p.OffsetY(1);
    CHECK_TRUE(p.Y() == 1);

    p.OffsetY(2);
    CHECK_TRUE(p.Y() == 3);

    p.OffsetY(-1);
    CHECK_TRUE(p.Y() == 2);

    p.OffsetY(-3);
    CHECK_TRUE(p.Y() == -1);
}

TEST_MEMBER_FUNCTION(Point, OffsetXY, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    ocl::Point<int> p;

    p.OffsetXY(1, 2);
    CHECK_TRUE(p.X() == 1);
    CHECK_TRUE(p.Y() == 2);

    p.OffsetXY(2, 3);
    CHECK_TRUE(p.X() == 3);
    CHECK_TRUE(p.Y() == 5);

    p.OffsetXY(-1, -2);
    CHECK_TRUE(p.X() == 2);
    CHECK_TRUE(p.Y() == 3);

    p.OffsetXY(-3, -5);
    CHECK_TRUE(p.X() == -1);
    CHECK_TRUE(p.Y() == -2);
}

TEST_MEMBER_FUNCTION(Point, Clear, NA)
{
    ocl::Point<int> p(1, 2);

    p.Clear();
    CHECK_TRUE(p.X() == 0);
    CHECK_TRUE(p.Y() == 0);
}

TEST_MEMBER_FUNCTION(Point, BoundsCheckX, int)
{
    {
        ocl::Point<signed char> p(126, 0);

        CHECK_TRUE(p.BoundsCheckX(1, true));
        CHECK_FALSE(p.BoundsCheckX(2, true));
        CHECK_TRUE(p.BoundsCheckX(-1, false));
        CHECK_FALSE(p.BoundsCheckX(-2, false));
    }

    {
        ocl::Point<signed char> p(-127, 0);

        CHECK_TRUE(p.BoundsCheckX(-1, true));
        CHECK_FALSE(p.BoundsCheckX(-2, true));
        CHECK_TRUE(p.BoundsCheckX(1, false));
        CHECK_FALSE(p.BoundsCheckX(2, false));
    }
}

TEST_MEMBER_FUNCTION(Point, BoundsCheckX, unsigned_int)
{
    TEST_OVERRIDE_ARGS("unsigned int");

    {
        ocl::Point<unsigned char> p(254U, 0U);

        CHECK_TRUE(p.BoundsCheckX(1, true));
        CHECK_FALSE(p.BoundsCheckX(2, true));
    }

    {
        ocl::Point<unsigned char> p(1U, 0U);

        CHECK_TRUE(p.BoundsCheckX(1, false));
        CHECK_FALSE(p.BoundsCheckX(2, false));
    }
}

TEST_MEMBER_FUNCTION(Point, BoundsCheckY, int)
{
    {
        ocl::Point<signed char> p(0, 126);

        CHECK_TRUE(p.BoundsCheckY(1, true));
        CHECK_FALSE(p.BoundsCheckY(2, true));
        CHECK_TRUE(p.BoundsCheckY(-1, false));
        CHECK_FALSE(p.BoundsCheckY(-2, false));
    }

    {
        ocl::Point<signed char> p(0, -127);

        CHECK_TRUE(p.BoundsCheckY(-1, true));
        CHECK_FALSE(p.BoundsCheckY(-2, true));
        CHECK_TRUE(p.BoundsCheckY(1, false));
        CHECK_FALSE(p.BoundsCheckY(2, false));
    }
}

TEST_MEMBER_FUNCTION(Point, BoundsCheckY, unsigned_int)
{
    TEST_OVERRIDE_ARGS("unsigned int");

    {
        ocl::Point<unsigned char> p(0U, 254U);

        CHECK_TRUE(p.BoundsCheckY(1, true));
        CHECK_FALSE(p.BoundsCheckY(2, true));
    }

    {
        ocl::Point<unsigned char> p(0U, 1U);

        CHECK_TRUE(p.BoundsCheckY(1, false));
        CHECK_FALSE(p.BoundsCheckY(2, false));
    }
}

TEST_MEMBER_FUNCTION(Point, BoundsCheckXY, int_int)
{
    TEST_OVERRIDE_ARGS("int, int");

    {
        ocl::Point<signed char> p(126, 0);

        CHECK_TRUE(p.BoundsCheckXY(1, 0, true));
        CHECK_FALSE(p.BoundsCheckXY(2, 0, true));
        CHECK_TRUE(p.BoundsCheckXY(-1, 0, false));
        CHECK_FALSE(p.BoundsCheckXY(-2, 0, false));
    }

    {
        ocl::Point<signed char> p(0, 126);

        CHECK_TRUE(p.BoundsCheckXY(0, 1, true));
        CHECK_FALSE(p.BoundsCheckXY(0, 2, true));
        CHECK_TRUE(p.BoundsCheckXY(0, -1, false));
        CHECK_FALSE(p.BoundsCheckXY(0, -2, false));
    }

    {
        ocl::Point<signed char> p(126, 126);

        CHECK_TRUE(p.BoundsCheckXY(1, 1, true));
        CHECK_FALSE(p.BoundsCheckXY(2, 2, true));
        CHECK_TRUE(p.BoundsCheckXY(-1, -1, false));
        CHECK_FALSE(p.BoundsCheckXY(-2, -2, false));
    }

    {
        ocl::Point<signed char> p(-127, 0);

        CHECK_TRUE(p.BoundsCheckXY(-1, 0, true));
        CHECK_FALSE(p.BoundsCheckXY(-2, 0, true));
        CHECK_TRUE(p.BoundsCheckXY(1, 0, false));
        CHECK_FALSE(p.BoundsCheckXY(2, 0, false));
    }

    {
        ocl::Point<signed char> p(0, -127);

        CHECK_TRUE(p.BoundsCheckXY(0, -1, true));
        CHECK_FALSE(p.BoundsCheckXY(0, -2, true));
        CHECK_TRUE(p.BoundsCheckXY(0, 1, false));
        CHECK_FALSE(p.BoundsCheckXY(0, 2, false));
    }

    {
        ocl::Point<signed char> p(-127, -127);

        CHECK_TRUE(p.BoundsCheckXY(-1, -1, true));
        CHECK_FALSE(p.BoundsCheckXY(-2, -2, true));
        CHECK_TRUE(p.BoundsCheckXY(1, 1, false));
        CHECK_FALSE(p.BoundsCheckXY(2, 2, false));
    }
}

TEST_MEMBER_FUNCTION(Point, BoundsCheckXY, unsigned_int_unsigned_int)
{
    TEST_OVERRIDE_ARGS("unsigned int, unsigned int");

    {
        ocl::Point<unsigned char> p(254U, 0U);

        CHECK_TRUE(p.BoundsCheckXY(1, 0, true));
        CHECK_FALSE(p.BoundsCheckXY(2, 0, true));
    }

    {
        ocl::Point<unsigned char> p(0U, 254U);

        CHECK_TRUE(p.BoundsCheckXY(0, 1, true));
        CHECK_FALSE(p.BoundsCheckXY(0, 2, true));
    }

    {
        ocl::Point<unsigned char> p(254U, 254U);

        CHECK_TRUE(p.BoundsCheckXY(1, 1, true));
        CHECK_FALSE(p.BoundsCheckXY(2, 2, true));
    }

    {
        ocl::Point<unsigned char> p(1U, 0U);

        CHECK_TRUE(p.BoundsCheckXY(1, 0, false));
        CHECK_FALSE(p.BoundsCheckXY(2, 0, false));
    }

    {
        ocl::Point<unsigned char> p(0U, 1U);

        CHECK_TRUE(p.BoundsCheckXY(0, 1, false));
        CHECK_FALSE(p.BoundsCheckXY(0, 2, false));
    }

    {
        ocl::Point<unsigned char> p(1U, 1U);

        CHECK_TRUE(p.BoundsCheckXY(1, 1, false));
        CHECK_FALSE(p.BoundsCheckXY(2, 2, false));
    }
}

TEST_MEMBER_FUNCTION(Point, SignedCast, Point)
{
    ocl::Point<unsigned int> p1(1U, 2U);
    ocl::Point<int> p2(p1.SignedCast());
    CHECK_TRUE(p2.X() == 1);
    CHECK_TRUE(p2.Y() == 2);
}

TEST_MEMBER_FUNCTION(Point, UnsignedCast, Point)
{
    ocl::Point<int> p1(1, 2);
    ocl::Point<unsigned int> p2(p1.UnsignedCast());
    CHECK_TRUE(p2.X() == 1U);
    CHECK_TRUE(p2.Y() == 2U);
}

TEST_MEMBER_FUNCTION(Point, GetDiff, Point)
{
    {
        typedef ocl::Point<int> point_type;

        point_type p1(1, 2);
        point_type p2(3, 5);

        point_type diff = p1.GetDiff(p2);
        CHECK_TRUE(diff.X() == -2);
        CHECK_TRUE(diff.Y() == -3);

        diff = p2.GetDiff(p1);
        CHECK_TRUE(diff.X() == 2);
        CHECK_TRUE(diff.Y() == 3);
    }

    {
        typedef ocl::Point<unsigned int> point_type;

        point_type p1(1, 2);
        point_type p2(3, 5);

        ocl::Point<int> diff = p1.GetDiff(p2);
        CHECK_TRUE(diff.X() == -2);
        CHECK_TRUE(diff.Y() == -3);

        diff = p2.GetDiff(p1);
        CHECK_TRUE(diff.X() == 2);
        CHECK_TRUE(diff.Y() == 3);
    }
}

TEST_MEMBER_FUNCTION(Point, GetAbsDiff, Point)
{
    {
        typedef ocl::Point<int> point_type;

        point_type p1(1, 2);
        point_type p2(3, 5);

        point_type diff = p1.GetAbsDiff(p2);
        CHECK_TRUE(diff.X() == 2);
        CHECK_TRUE(diff.Y() == 3);

        diff = p2.GetAbsDiff(p1);
        CHECK_TRUE(diff.X() == 2);
        CHECK_TRUE(diff.Y() == 3);
    }

    {
        typedef ocl::Point<unsigned int> point_type;

        point_type p1(1, 2);
        point_type p2(3, 5);

        point_type diff = p1.GetAbsDiff(p2);
        CHECK_TRUE(diff.X() == 2);
        CHECK_TRUE(diff.Y() == 3);

        diff = p2.GetAbsDiff(p1);
        CHECK_TRUE(diff.X() == 2);
        CHECK_TRUE(diff.Y() == 3);
    }
}

TEST_MEMBER_FUNCTION(Point, Swap, Point_ref)
{
    TEST_OVERRIDE_ARGS("Point&");

    typedef ocl::Point<int> point_type;

    point_type pt1(1, 2);
    point_type pt2(3, 4);

    pt1.Swap(pt2);

    CHECK_TRUE(pt1.X() == 3);
    CHECK_TRUE(pt1.Y() == 4);
    CHECK_TRUE(pt2.X() == 1);
    CHECK_TRUE(pt2.Y() == 2);
}
