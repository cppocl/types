/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/SimpleBuffer.hpp"
#include <cstddef>

TEST_MEMBER_FUNCTION(SimpleBuffer, SimpleBuffer, NA)
{
    ocl::SimpleBuffer<std::int32_t> buffer;

    CHECK_TRUE(buffer.GetSize() == 0U);
    CHECK_TRUE(buffer.GetPointer() == nullptr);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, SimpleBuffer, SimpleBuffer_const_ref)
{
    TEST_OVERRIDE_ARGS("SimpleBuffer const&");

    ocl::SimpleBuffer<char> buffer1(6U);
    memcpy(buffer1.GetPointer(), "hello", 6U);

    ocl::SimpleBuffer<char> buffer2(buffer1);

    CHECK_TRUE(buffer1.GetSize() == 6U);
    CHECK_TRUE(buffer1.GetPointer() != nullptr && StrCmp(buffer1.GetPointer(), "hello") == 0);
    CHECK_TRUE(buffer2.GetSize() == 6U);
    CHECK_TRUE(buffer2.GetPointer() != nullptr && StrCmp(buffer2.GetPointer(), "hello") == 0);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, SimpleBuffer, SimpleBuffer_move)
{
    TEST_OVERRIDE_ARGS("SimpleBuffer&&");

    ocl::SimpleBuffer<char> buffer1(6U);
    memcpy(buffer1.GetPointer(), "Hello", 6U);

    ocl::SimpleBuffer<char> buffer2(std::move(buffer1));

    CHECK_TRUE(buffer1.GetSize() == 0U);
    CHECK_TRUE(buffer1.GetPointer() == nullptr);
    CHECK_TRUE(buffer2.GetSize() == 6U);
    CHECK_TRUE(buffer2.GetPointer() != nullptr && StrCmp(buffer2.GetPointer(), "Hello") == 0);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, SimpleBuffer, size_type)
{
    ocl::SimpleBuffer<std::int32_t> buffer(4U);

    CHECK_TRUE(buffer.GetSize() == 4U);
    CHECK_TRUE(buffer.GetPointer() != nullptr);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, SimpleBuffer, char_const_ptr_size_type)
{
    TEST_OVERRIDE_ARGS("char const*, size_type");

    ocl::SimpleBuffer<char> buffer("Hello", 6U);

    CHECK_TRUE(buffer.GetSize() == 6U);
    CHECK_TRUE(buffer.GetPointer() != nullptr && StrCmp(buffer.GetPointer(), "Hello") == 0);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, operator_eq, SimpleBuffer_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator =", "SimpleBuffer const&");

    int ints[] = { 1, 2, 3, 4, 5, 6 };
    ocl::SimpleBuffer<int> buffer1(ints, 6U);
    ocl::SimpleBuffer<int> buffer2;

    buffer2 = buffer1;
    for (std::size_t pos = 0; pos < 6U; ++pos)
        CHECK_TRUE(buffer1[pos] == buffer2[pos]);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, operator_eq, SimpleBuffer_move)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator =", "SimpleBuffer&&");

    int ints[] = { 1, 2, 3, 4, 5, 6 };
    ocl::SimpleBuffer<int> buffer1(ints, 6U);
    ocl::SimpleBuffer<int> buffer2;

    buffer2 = std::move(buffer1);
    for (std::size_t pos = 0; pos < 6U; ++pos)
        CHECK_TRUE(buffer2[pos] == ints[pos]);

    CHECK_TRUE(buffer1.IsEmpty());
}

TEST_MEMBER_FUNCTION(SimpleBuffer, subscript, size_type)
{
    TEST_OVERRIDE_FUNCTION_NAME("operator []");

    // Test operator const version.
    {
        int ints[] = { 1, 2, 3, 4, 5, 6 };
        ocl::SimpleBuffer<int> const buffer(ints, 6U);
        for (std::size_t pos = 0; pos < 6U; ++pos)
            CHECK_TRUE(buffer[pos] == ints[pos]);
    }

    // Test operator non-const version.
    {
        int ints[] = { 1, 2, 3, 4, 5, 6 };
        ocl::SimpleBuffer<int> buffer(ints, 6U);
        for (std::size_t pos = 0; pos < 6U; ++pos)
        {
            CHECK_TRUE(buffer[pos] == ints[pos]);
            buffer[pos] = ints[pos] + 10;
        }

        for (std::size_t pos = 0; pos < 6U; ++pos)
            CHECK_TRUE(buffer[pos] == ints[pos] + 10);
    }
}

TEST_MEMBER_FUNCTION(SimpleBuffer, GetSize, NA)
{
    ocl::SimpleBuffer<char> buffer("Hello", 6U);

    CHECK_TRUE(buffer.GetSize() == 6U);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, GetFirst, NA)
{
    ocl::SimpleBuffer<char> buffer("Hello", 6U);

    CHECK_TRUE(buffer.GetFirst() == 'H');
}

TEST_MEMBER_FUNCTION(SimpleBuffer, GetLast, NA)
{
    ocl::SimpleBuffer<char> buffer("Hello", 6U);
    buffer[5U] = 'x';

    CHECK_TRUE(buffer.GetLast() == 'x');
}

TEST_MEMBER_FUNCTION(SimpleBuffer, IsEmpty, NA)
{
    ocl::SimpleBuffer<char> buffer1;
    ocl::SimpleBuffer<char> buffer2("Hello", 6U);

    CHECK_TRUE(buffer1.IsEmpty());
    CHECK_TRUE(!buffer2.IsEmpty());
}

TEST_MEMBER_FUNCTION(SimpleBuffer, GetPointer, NA)
{
    ocl::SimpleBuffer<char> buffer1;
    ocl::SimpleBuffer<char> buffer2("Hello", 6U);

    CHECK_TRUE(buffer1.GetPointer() == nullptr);
    CHECK_TRUE(buffer2.GetPointer() != nullptr);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Resize, size_type_bool_bool)
{
    TEST_OVERRIDE_ARGS("size_type, bool, bool");

    ocl::SimpleBuffer<char> buffer("ABC", 4U);

    // Check "ABC" is copied and rest of buffer is cleared.
    buffer.Resize(6U, true, true);
    CHECK_TRUE(MemCmp(buffer.GetPointer(), "ABC", 4U) == 0);
    CHECK_TRUE(buffer[4] == '\0');
    CHECK_TRUE(buffer[5] == '\0');

    // Shrink buffer and check "ABCD" still remains.
    MemCpy(buffer.GetPointer(), "ABCDE", 6U);
    buffer.Resize(4U, true, false);
    CHECK_TRUE(MemCmp(buffer.GetPointer(), "ABCD", 4U) == 0);

    // Check everything is cleared on resize and original string is lost.
    buffer.Resize(8U, false, true);
    for (std::size_t pos = 0; pos < 8U; ++pos)
        CHECK_TRUE(buffer[pos] == '\0');
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Grow, size_type_bool_bool)
{
    TEST_OVERRIDE_ARGS("size_type, bool, bool");

    ocl::SimpleBuffer<char> buffer;

    // Check "ABC" is copied and rest of buffer is cleared.
    buffer.Grow(4U, true, true);
    MemCpy(buffer.GetPointer(), "ABC", 4U);
    CHECK_TRUE(MemCmp(buffer.GetPointer(), "ABC", 4U) == 0);

    // Check "ABC" is copied and rest of buffer is cleared.
    buffer.Grow(6U, true, true);
    CHECK_TRUE(MemCmp(buffer.GetPointer(), "ABC", 4U) == 0);
    CHECK_TRUE(buffer[4] == '\0');
    CHECK_TRUE(buffer[5] == '\0');

    // Check everything is cleared on grow and original string is lost.
    buffer.Grow(8U, false, true);
    for (std::size_t pos = 0; pos < 8U; ++pos)
        CHECK_TRUE(buffer[pos] == '\0');
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Shrink, size_type_bool)
{
    TEST_OVERRIDE_ARGS("size_type, bool");

    ocl::SimpleBuffer<char> buffer("ABCDE", 6U);

    // Check shrinking keeps the remaining characters.
    buffer.Shrink(2U, true, true);
    CHECK_TRUE(buffer.GetSize() == 4U);
    CHECK_TRUE(MemCmp(buffer.GetPointer(), "ABCD", 4U) == 0);

    // Check shrinking throws away the remaining characters.
    buffer.Shrink(2U, false, true);
    CHECK_TRUE(buffer.GetSize() == 2U);
    CHECK_TRUE(buffer[0] == '\0');
    CHECK_TRUE(buffer[1] == '\0');
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Clear, NA)
{
    ocl::SimpleBuffer<char> buffer("ABCDE", 6U);

    buffer.Clear();
    CHECK_TRUE(buffer.GetSize() == 0U);
    CHECK_TRUE(buffer.GetPointer() == nullptr);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Fill, int32_t)
{
    ocl::SimpleBuffer<char> buffer("ABCDE", 6U);

    buffer.Fill('a');
    CHECK_TRUE(MemCmp(buffer.GetPointer(), "aaaaaa", 6U) == 0);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Set, size_type_size_type_int32_t)
{
    TEST_OVERRIDE_ARGS("size_type, size_type, int32_t");

    ocl::SimpleBuffer<char> buffer("ABCDE", 6U);

    buffer.Set(0, 5, 'a');
    CHECK_TRUE(MemCmp(buffer.GetPointer(), "aaaaaa", 6U) == 0);

}

TEST_MEMBER_FUNCTION(SimpleBuffer, Copy, SimpleBuffer_const_ref)
{
    TEST_OVERRIDE_ARGS("SimpleBuffer const&");

    int ints[] = { 1, 2, 3, 4, 5, 6 };
    ocl::SimpleBuffer<int> buffer1(ints, 6U);
    ocl::SimpleBuffer<int> buffer2;

    buffer2.Copy(buffer1);
    for (std::size_t pos = 0; pos < 6U; ++pos)
        CHECK_TRUE(buffer1[pos] == buffer2[pos]);
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Move, SimpleBuffer_ref)
{
    TEST_OVERRIDE_ARGS("SimpleBuffer&");

    int ints[] = { 1, 2, 3, 4, 5, 6 };
    ocl::SimpleBuffer<int> buffer1(ints, 6U);
    ocl::SimpleBuffer<int> buffer2;

    buffer2.Move(buffer1);
    for (std::size_t pos = 0; pos < 6U; ++pos)
        CHECK_TRUE(buffer2[pos] == ints[pos]);

    CHECK_TRUE(buffer1.IsEmpty());
}

TEST_MEMBER_FUNCTION(SimpleBuffer, Swap, SimpleBuffer_ref)
{
    TEST_OVERRIDE_ARGS("SimpleBuffer&");

    int ints1[] = { 1, 2, 3, 4, 5, 6 };
    int ints2[] = { 11, 12, 13, 14, 15, 16 };
    ocl::SimpleBuffer<int> buffer1(ints1, 6U);
    ocl::SimpleBuffer<int> buffer2(ints2, 6U);

    buffer1.Swap(buffer2);

    for (std::size_t pos = 0; pos < 6U; ++pos)
    {
        CHECK_TRUE(buffer1[pos] == ints2[pos]);
        CHECK_TRUE(buffer2[pos] == ints1[pos]);
    }
}
