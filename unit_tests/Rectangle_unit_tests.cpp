/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../Rectangle.hpp"
#include <utility>

TEST_MEMBER_FUNCTION(Rectangle, Rectangle, NA)
{
    typedef ocl::TypeTraits<int> type_traits;

    ocl::Rectangle<int> rect;

    CHECK_TRUE(rect.Left() == type_traits::Default());
    CHECK_TRUE(rect.Top() == type_traits::Default());
    CHECK_TRUE(rect.Right() == type_traits::Default());
    CHECK_TRUE(rect.Bottom() == type_traits::Default());
}

TEST_MEMBER_FUNCTION(Rectangle, Rectangle, int_int_int_int)
{
    TEST_OVERRIDE_ARGS("int, int, int, int");

    typedef ocl::TypeTraits<int> type_traits;

    ocl::Rectangle<int> rect(1, 2, 3, 4);

    CHECK_TRUE(rect.Left() == 1);
    CHECK_TRUE(rect.Top() == 2);
    CHECK_TRUE(rect.Right() == 3);
    CHECK_TRUE(rect.Bottom() == 4);
}

TEST_MEMBER_FUNCTION(Rectangle, Rectangle, Rectangle_const_ref)
{
    TEST_OVERRIDE_ARGS("Rectangle const&");

    ocl::Rectangle<int> rc1(1, 2, 3, 4);
    ocl::Rectangle<int> rc2(rc1);

    CHECK_TRUE(rc1.Left() == rc2.Left());
    CHECK_TRUE(rc1.Top() == rc2.Top());
    CHECK_TRUE(rc1.Right() == rc2.Right());
    CHECK_TRUE(rc1.Bottom() == rc2.Bottom());
}

TEST_MEMBER_FUNCTION(Rectangle, Rectangle, Rectangle_move)
{
    TEST_OVERRIDE_ARGS("Rectangle&&");

    ocl::Rectangle<int> rc1(1, 2, 3, 4);
    ocl::Rectangle<int> rc2(std::move(rc1));

    CHECK_TRUE(rc1.Left() == rc2.Left());
    CHECK_TRUE(rc1.Top() == rc2.Top());
    CHECK_TRUE(rc1.Right() == rc2.Right());
    CHECK_TRUE(rc1.Bottom() == rc2.Bottom());
}

TEST_MEMBER_FUNCTION(Rectangle, operator_eq, Rectangle_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator =", "Rectangle const&");

    ocl::Rectangle<int> rc1(1, 2, 3, 4);
    ocl::Rectangle<int> rc2;

    rc2 = rc1;
    CHECK_TRUE(rc1.Left() == rc2.Left());
    CHECK_TRUE(rc1.Top() == rc2.Top());
    CHECK_TRUE(rc1.Right() == rc2.Right());
    CHECK_TRUE(rc1.Bottom() == rc2.Bottom());
}

TEST_MEMBER_FUNCTION(Rectangle, operator_plus_eq, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator +=", "Rectangle const&");

    ocl::Rectangle<int> rect(1, 2, 3, 4);
    rect += ocl::Point<int>(10, 20);

    CHECK_TRUE(rect.Left() == 11);
    CHECK_TRUE(rect.Top() == 22);
    CHECK_TRUE(rect.Right() == 13);
    CHECK_TRUE(rect.Bottom() == 24);
}

TEST_MEMBER_FUNCTION(Rectangle, operator_minus_eq, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator -=", "Rectangle const&");

    ocl::Rectangle<int> rect(21, 22, 23, 24);
    rect -= ocl::Point<int>(10, 20);

    CHECK_TRUE(rect.Left() == 11);
    CHECK_TRUE(rect.Top() == 2);
    CHECK_TRUE(rect.Right() == 13);
    CHECK_TRUE(rect.Bottom() == 4);
}

TEST_MEMBER_FUNCTION(Rectangle, operator_multiply_eq, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator *=", "Rectangle const&");

    ocl::Rectangle<int> rect(1, 2, 3, 4);
    rect *= ocl::Point<int>(10, 20);

    CHECK_TRUE(rect.Left() == 10);
    CHECK_TRUE(rect.Top() == 40);
    CHECK_TRUE(rect.Right() == 30);
    CHECK_TRUE(rect.Bottom() == 80);
}

TEST_MEMBER_FUNCTION(Rectangle, operator_divide_eq, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator /=", "Rectangle const&");

    ocl::Rectangle<int> rect(10, 20, 30, 40);
    rect /= ocl::Point<int>(2, 20);

    CHECK_TRUE(rect.Left() == 5);
    CHECK_TRUE(rect.Top() == 1);
    CHECK_TRUE(rect.Right() == 15);
    CHECK_TRUE(rect.Bottom() == 2);
}

TEST_MEMBER_FUNCTION(Rectangle, operator_plus, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator +", "Rectangle const&");

    ocl::Rectangle<int> rc1(1, 2, 3, 4);
    ocl::Rectangle<int> rc2 = rc1 + ocl::Point<int>(10, 20);

    CHECK_TRUE(rc2.Left() == 11);
    CHECK_TRUE(rc2.Top() == 22);
    CHECK_TRUE(rc2.Right() == 13);
    CHECK_TRUE(rc2.Bottom() == 24);
}

TEST_MEMBER_FUNCTION(Rectangle, operator_minus, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator -", "Rectangle const&");

    ocl::Rectangle<int> rc1(21, 22, 23, 24);
    ocl::Rectangle<int> rc2 = rc1 - ocl::Point<int>(10, 20);

    CHECK_TRUE(rc2.Left() == 11);
    CHECK_TRUE(rc2.Top() == 2);
    CHECK_TRUE(rc2.Right() == 13);
    CHECK_TRUE(rc2.Bottom() == 4);
}

TEST_MEMBER_FUNCTION(Rectangle, operator_multiply, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator *", "Rectangle const&");

    ocl::Rectangle<int> rc1(1, 2, 3, 4);
    ocl::Rectangle<int> rc2 = rc1 * ocl::Point<int>(10, 20);

    CHECK_TRUE(rc2.Left() == 10);
    CHECK_TRUE(rc2.Top() == 40);
    CHECK_TRUE(rc2.Right() == 30);
    CHECK_TRUE(rc2.Bottom() == 80);
}

TEST_MEMBER_FUNCTION(Rectangle, operator_divide, Point_const_ref)
{
    TEST_OVERRIDE_FUNCTION_NAME_ARGS("operator /", "Rectangle const&");

    ocl::Rectangle<int> rc1(10, 20, 30, 40);
    ocl::Rectangle<int> rc2 = rc1 / ocl::Point<int>(2, 20);

    CHECK_TRUE(rc2.Left() == 5);
    CHECK_TRUE(rc2.Top() == 1);
    CHECK_TRUE(rc2.Right() == 15);
    CHECK_TRUE(rc2.Bottom() == 2);
}

TEST_MEMBER_FUNCTION(Rectangle, Left, NA)
{
    ocl::Rectangle<int> rect(1, 2, 3, 4);
    CHECK_TRUE(rect.Left() == 1);
}

TEST_MEMBER_FUNCTION(Rectangle, Top, NA)
{
    ocl::Rectangle<int> rect(1, 2, 3, 4);
    CHECK_TRUE(rect.Top() == 2);
}

TEST_MEMBER_FUNCTION(Rectangle, Right, NA)
{
    ocl::Rectangle<int> rect(1, 2, 3, 4);
    CHECK_TRUE(rect.Right() == 3);
}

TEST_MEMBER_FUNCTION(Rectangle, Bottom, NA)
{
    ocl::Rectangle<int> rect(1, 2, 3, 4);
    CHECK_TRUE(rect.Bottom() == 4);
}

TEST_MEMBER_FUNCTION(Rectangle, TopLeft, NA)
{
    ocl::Rectangle<int> rect(1, 2, 3, 4);
    CHECK_TRUE(rect.TopLeft() == ocl::Point<int>(1, 2));
}

TEST_MEMBER_FUNCTION(Rectangle, BottomRight, NA)
{
    ocl::Rectangle<int> rect(1, 2, 3, 4);
    CHECK_TRUE(rect.BottomRight() == ocl::Point<int>(3, 4));
}

TEST_MEMBER_FUNCTION(Rectangle, Copy, Rectangle_const_ref)
{
    TEST_OVERRIDE_ARGS("Rectangle const&");
    ocl::Rectangle<int> rc1(1, 2, 3, 4);
    ocl::Rectangle<int> rc2;

    rc2.Copy(rc1);
    CHECK_TRUE(rc2.Left() == 1);
    CHECK_TRUE(rc2.Top() == 2);
    CHECK_TRUE(rc2.Right() == 3);
    CHECK_TRUE(rc2.Bottom() == 4);
}

TEST_MEMBER_FUNCTION(Rectangle, Move, Rectangle_ref)
{
    TEST_OVERRIDE_ARGS("Rectangle&");
    ocl::Rectangle<int> rc1(1, 2, 3, 4);
    ocl::Rectangle<int> rc2;

    rc2.Move(rc1);
    CHECK_TRUE(rc2.Left() == 1);
    CHECK_TRUE(rc2.Top() == 2);
    CHECK_TRUE(rc2.Right() == 3);
    CHECK_TRUE(rc2.Bottom() == 4);
}

TEST_MEMBER_FUNCTION(Rectangle, GetSize, NA)
{
    ocl::Rectangle<int> rect(1, 1, 2, 3);
    ocl::Size<int> size(rect.GetSize());

    CHECK_TRUE(size.Width() == 2);
    CHECK_TRUE(size.Height() == 3);
}

TEST_MEMBER_FUNCTION(Rectangle, IsIntersection, Rectangle_const_ref)
{
    TEST_OVERRIDE_ARGS("Rectangle const&");

    typedef ocl::Rectangle<int> rectange_type;

    {
        // Test top left overlap.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(1, 1, 3, 3);
        CHECK_TRUE(rc1.IsIntersection(rc2));

        // Test overlapping single pixel on top left corner.
        rectange_type rc3(1, 1, 2, 2);
        CHECK_TRUE(rc1.IsIntersection(rc3));
    }

    {
        // Test top right overlap.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(3, 1, 5, 3);
        CHECK_TRUE(rc1.IsIntersection(rc2));

        // Test overlapping single pixel on top right corner.
        rectange_type rc3(4, 1, 5, 2);
        CHECK_TRUE(rc1.IsIntersection(rc3));
    }

    {
        // Test bottom left overlap.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(1, 3, 3, 5);
        CHECK_TRUE(rc1.IsIntersection(rc2));

        // Test overlapping single pixel on bottom left corner.
        rectange_type rc3(2, 3, 3, 4);
        CHECK_TRUE(rc1.IsIntersection(rc3));
    }

    {
        // Test bottom right overlap.
        rectange_type rc1(1, 1, 3, 3);
        rectange_type rc2(2, 2, 4, 4);
        CHECK_TRUE(rc1.IsIntersection(rc2));

        // Test overlapping single pixel on bottom right corner.
        rectange_type rc3(3, 3, 4, 4);
        CHECK_TRUE(rc1.IsIntersection(rc3));
    }
}

TEST_MEMBER_FUNCTION(Rectangle, GetIntersection, Rectangle_const_ref)
{
    TEST_OVERRIDE_ARGS("Rectangle const&");

    typedef ocl::Rectangle<int> rectange_type;

    {
        // Test top left intersection.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(1, 1, 3, 3);
        CHECK_TRUE(rc1.GetIntersection(rc2) == rectange_type(2, 2, 3, 3));

        // Test single pixel on top left corner intersection.
        rectange_type rc3(1, 1, 2, 2);
        CHECK_TRUE(rc1.GetIntersection(rc3) == rectange_type(2, 2, 2, 2));
    }

    {
        // Test top right intersection.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(3, 1, 5, 3);
        CHECK_TRUE(rc1.GetIntersection(rc2) == rectange_type(3, 2, 4, 3));

        // Test single pixel on top right corner intersection.
        rectange_type rc3(4, 1, 5, 2);
        CHECK_TRUE(rc1.GetIntersection(rc3) == rectange_type(4, 2, 4, 2));
    }

    {
        // Test bottom left overlap.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(1, 3, 3, 5);
        CHECK_TRUE(rc1.GetIntersection(rc2) == rectange_type(2, 3, 3, 4));

        // Test overlapping single pixel on bottom left corner.
        rectange_type rc3(2, 3, 3, 4);
        CHECK_TRUE(rc1.GetIntersection(rc3) == rectange_type(2, 3, 3, 4));
    }

    {
        // Test bottom right overlap.
        rectange_type rc1(1, 1, 3, 3);
        rectange_type rc2(2, 2, 4, 4);
        CHECK_TRUE(rc1.GetIntersection(rc2) == rectange_type(2, 2, 3, 3));

        // Test overlapping single pixel on bottom right corner.
        rectange_type rc3(3, 3, 4, 4);
        CHECK_TRUE(rc1.GetIntersection(rc3) == rectange_type(3, 3, 3, 3));
    }
}

TEST_MEMBER_FUNCTION(Rectangle, GetPerimeter, Rectangle_const_ref)
{
    TEST_OVERRIDE_ARGS("Rectangle const&");

    typedef ocl::Rectangle<int> rectange_type;

    {
        // Test top left intersection.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(1, 1, 3, 3);
        CHECK_TRUE(rc1.GetPerimeter(rc2) == rectange_type(1, 1, 4, 4));
    }

    {
        // Test top right intersection.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(3, 1, 5, 3);
        CHECK_TRUE(rc1.GetPerimeter(rc2) == rectange_type(2, 1, 5, 4));
    }

    {
        // Test bottom left overlap.
        rectange_type rc1(2, 2, 4, 4);
        rectange_type rc2(1, 3, 3, 5);
        CHECK_TRUE(rc1.GetPerimeter(rc2) == rectange_type(1, 2, 4, 5));
    }

    {
        // Test bottom right overlap.
        rectange_type rc1(1, 1, 3, 3);
        rectange_type rc2(2, 2, 4, 4);
        CHECK_TRUE(rc1.GetPerimeter(rc2) == rectange_type(1, 1, 4, 4));
    }
}

TEST_MEMBER_FUNCTION(Rectangle, Swap, Rectangle_ref)
{
    TEST_OVERRIDE_ARGS("Rectangle&");

    ocl::Rectangle<int> rect1(1, 2, 3, 4);
    ocl::Rectangle<int> rect2(5, 6, 7, 8);

    rect1.Swap(rect2);

    CHECK_TRUE(rect1.Left() == 5);
    CHECK_TRUE(rect1.Top() == 6);
    CHECK_TRUE(rect1.Right() == 7);
    CHECK_TRUE(rect1.Bottom() == 8);
    CHECK_TRUE(rect2.Left() == 1);
    CHECK_TRUE(rect2.Top() == 2);
    CHECK_TRUE(rect2.Right() == 3);
    CHECK_TRUE(rect2.Bottom() == 4);
}
