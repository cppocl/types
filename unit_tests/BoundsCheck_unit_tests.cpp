/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/BoundsCheck.hpp"
#include "../internal/TypeTraits.hpp"

TEST(BoundsCheck)
{
    typedef ocl::BoundsCheck<int> bounds_check;
    typedef ocl::TypeTraits<int> type_traits;
    constexpr int Min = type_traits::Min();
    constexpr int Max = type_traits::Max();

    CHECK_TRUE(bounds_check::CanAdd(0, 0));
    CHECK_TRUE(bounds_check::CanAdd(0, 1));
    CHECK_TRUE(bounds_check::CanAdd(1, 0));

    CHECK_TRUE(bounds_check::CanAdd(1, 1));
    CHECK_TRUE(bounds_check::CanAdd(1, -1));
    CHECK_TRUE(bounds_check::CanAdd(-1, 1));
    CHECK_TRUE(bounds_check::CanAdd(-1, -1));

    CHECK_TRUE(bounds_check::CanAdd(Min + 1, -1));
    CHECK_TRUE(bounds_check::CanAdd(Min, 1));
    CHECK_TRUE(bounds_check::CanAdd(Max - 1, 1));
    CHECK_TRUE(bounds_check::CanAdd(Max, -1));
    CHECK_TRUE(bounds_check::CanAdd(-1, Min + 1));
    CHECK_TRUE(bounds_check::CanAdd(1, Min));
    CHECK_TRUE(bounds_check::CanAdd(1, Max - 1));
    CHECK_TRUE(bounds_check::CanAdd(-1, Max));

    CHECK_TRUE(bounds_check::CanAdd(Min, 0));
    CHECK_TRUE(bounds_check::CanAdd(Max, 0));
    CHECK_TRUE(bounds_check::CanAdd(0, Min));
    CHECK_TRUE(bounds_check::CanAdd(0, Max));

    CHECK_FALSE(bounds_check::CanAdd(Min, -1));
    CHECK_FALSE(bounds_check::CanAdd(Max, 1));
}
