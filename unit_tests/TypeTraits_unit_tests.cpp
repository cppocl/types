/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/TypeTraits.hpp"
#include <string>

TEST(TypeTraits_char_tests)
{
    typedef char type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "char";
    std::string no_sign_type_string = "char";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0);
    CHECK_TRUE(type_traits::ONE == 1);
    CHECK_TRUE(type_traits::MINUS_ONE == static_cast<char>(-1));
    CHECK_TRUE(type_traits::MIN == SCHAR_MIN);
    CHECK_TRUE(type_traits::MAX == SCHAR_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == static_cast<type>(0x40));
    CHECK_TRUE(type_traits::ALL_BITS == static_cast<type>(0xff));
    CHECK_TRUE(type_traits::DEFAULT == 0);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::MinusOne() == type_traits::MINUS_ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::TopBit() == static_cast<type>(0x40));
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_signed_char_tests)
{
    typedef signed char type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "signed char";
    std::string no_sign_type_string = "char";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0);
    CHECK_TRUE(type_traits::ONE == 1);
    CHECK_TRUE(type_traits::MINUS_ONE == static_cast<char>(-1));
    CHECK_TRUE(type_traits::MIN == SCHAR_MIN);
    CHECK_TRUE(type_traits::MAX == SCHAR_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == static_cast<type>(0x40));
    CHECK_TRUE(type_traits::ALL_BITS == static_cast<type>(0xff));
    CHECK_TRUE(type_traits::DEFAULT == 0);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::MinusOne() == type_traits::MINUS_ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::TopBit() == static_cast<type>(0x40));
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_unsigned_char_tests)
{
    typedef unsigned char type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "unsigned char";
    std::string no_sign_type_string = "char";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_FALSE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0U);
    CHECK_TRUE(type_traits::ONE == 1U);
    CHECK_TRUE(type_traits::MIN == 0U);
    CHECK_TRUE(type_traits::MAX == UCHAR_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == static_cast<type>(0x80));
    CHECK_TRUE(type_traits::ALL_BITS == static_cast<type>(0xff));
    CHECK_TRUE(type_traits::DEFAULT == 0U);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::TopBit() == static_cast<type>(0x80U));
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_signed_short_tests)
{
    typedef signed short type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "signed short";
    std::string no_sign_type_string = "short";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0);
    CHECK_TRUE(type_traits::ONE == 1);
    CHECK_TRUE(type_traits::MINUS_ONE == static_cast<char>(-1));
    CHECK_TRUE(type_traits::MIN == SHRT_MIN);
    CHECK_TRUE(type_traits::MAX == SHRT_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == static_cast<type>(USHRT_MAX));
    CHECK_TRUE(type_traits::DEFAULT == 0);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::MinusOne() == type_traits::MINUS_ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_unsigned_short_tests)
{
    typedef unsigned short type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "unsigned short";
    std::string no_sign_type_string = "short";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_FALSE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0U);
    CHECK_TRUE(type_traits::ONE == 1U);
    CHECK_TRUE(type_traits::MIN == 0U);
    CHECK_TRUE(type_traits::MAX == USHRT_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == USHRT_MAX);
    CHECK_TRUE(type_traits::DEFAULT == 0U);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_signed_int_tests)
{
    typedef signed int type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "signed int";
    std::string no_sign_type_string = "int";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0);
    CHECK_TRUE(type_traits::ONE == 1);
    CHECK_TRUE(type_traits::MINUS_ONE == static_cast<char>(-1));
    CHECK_TRUE(type_traits::MIN == INT_MIN);
    CHECK_TRUE(type_traits::MAX == INT_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == static_cast<type>(UINT_MAX));
    CHECK_TRUE(type_traits::DEFAULT == 0);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::MinusOne() == type_traits::MINUS_ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_unsigned_int_tests)
{
    typedef unsigned int type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "unsigned int";
    std::string no_sign_type_string = "int";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_FALSE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0U);
    CHECK_TRUE(type_traits::ONE == 1U);
    CHECK_TRUE(type_traits::MIN == 0U);
    CHECK_TRUE(type_traits::MAX == UINT_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == UINT_MAX);
    CHECK_TRUE(type_traits::DEFAULT == 0U);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_signed_long_tests)
{
    typedef signed long type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "signed long";
    std::string no_sign_type_string = "long";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0);
    CHECK_TRUE(type_traits::ONE == 1);
    CHECK_TRUE(type_traits::MINUS_ONE == static_cast<char>(-1));
    CHECK_TRUE(type_traits::MIN == LONG_MIN);
    CHECK_TRUE(type_traits::MAX == LONG_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == static_cast<type>(ULONG_MAX));
    CHECK_TRUE(type_traits::DEFAULT == 0);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::MinusOne() == type_traits::MINUS_ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_unsigned_long_tests)
{
    typedef unsigned long type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "unsigned long";
    std::string no_sign_type_string = "long";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_FALSE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0U);
    CHECK_TRUE(type_traits::ONE == 1U);
    CHECK_TRUE(type_traits::MIN == 0U);
    CHECK_TRUE(type_traits::MAX == ULONG_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == ULONG_MAX);
    CHECK_TRUE(type_traits::DEFAULT == 0U);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_signed_long_long_tests)
{
    typedef signed long long type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "signed long long";
    std::string no_sign_type_string = "long long";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0);
    CHECK_TRUE(type_traits::ONE == 1);
    CHECK_TRUE(type_traits::MINUS_ONE == static_cast<char>(-1));
    CHECK_TRUE(type_traits::MIN == LLONG_MIN);
    CHECK_TRUE(type_traits::MAX == LLONG_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == static_cast<type>(ULLONG_MAX));
    CHECK_TRUE(type_traits::DEFAULT == 0);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::MinusOne() == type_traits::MINUS_ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_unsigned_long_long_tests)
{
    typedef unsigned long long type;
    typedef ocl::TypeTraits<type> type_traits;
    typedef ocl::TypeTraits<typename type_traits::signed_type> signed_type_traits;
    typedef ocl::TypeTraits<typename type_traits::unsigned_type> unsigned_type_traits;

    std::string type_string         = "unsigned long long";
    std::string no_sign_type_string = "long long";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_FALSE(type_traits::IS_SIGNED);
    CHECK_TRUE(type_traits::ZERO == 0U);
    CHECK_TRUE(type_traits::ONE == 1U);
    CHECK_TRUE(type_traits::MIN == 0U);
    CHECK_TRUE(type_traits::MAX == ULLONG_MAX);
    CHECK_TRUE(type_traits::TOP_BIT == type_traits::MAX - (type_traits::MAX >> 1));
    CHECK_TRUE(type_traits::ALL_BITS == ULLONG_MAX);
    CHECK_TRUE(type_traits::DEFAULT == 0U);

    CHECK_TRUE(type_traits::Zero() == type_traits::ZERO);
    CHECK_TRUE(type_traits::One() == type_traits::ONE);
    CHECK_TRUE(type_traits::Min() == type_traits::MIN);
    CHECK_TRUE(type_traits::Max() == type_traits::MAX);
    CHECK_TRUE(type_traits::Default() == type_traits::ZERO);

    CHECK_TRUE(type_string == type_traits::TypeString());
    CHECK_TRUE("signed " + no_sign_type_string == signed_type_traits::TypeString());
    CHECK_TRUE("unsigned " + no_sign_type_string == unsigned_type_traits::TypeString());
}

TEST(TypeTraits_float_tests)
{
    typedef float type;
    typedef ocl::TypeTraits<type> type_traits;

    std::string type_string = "float";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);

    CHECK_TRUE(type_traits::Zero() == 0.0f);
    CHECK_TRUE(type_traits::One() == 1.0f);
    CHECK_TRUE(type_traits::MinusOne() == -1.0f);
    CHECK_TRUE(type_traits::Min() == FLT_MAX * -1.0f);
    CHECK_TRUE(type_traits::Max() == FLT_MAX);
    CHECK_TRUE(type_traits::Default() == 0.0f);

    CHECK_TRUE(type_string == type_traits::TypeString());
}

TEST(TypeTraits_double_tests)
{
    typedef double type;
    typedef ocl::TypeTraits<type> type_traits;

    std::string type_string = "double";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);

    CHECK_TRUE(type_traits::Zero() == 0.0);
    CHECK_TRUE(type_traits::One() == 1.0);
    CHECK_TRUE(type_traits::MinusOne() == -1.0);
    CHECK_TRUE(type_traits::Min() == DBL_MAX * -1.0);
    CHECK_TRUE(type_traits::Max() == DBL_MAX);
    CHECK_TRUE(type_traits::Default() == 0.0);

    CHECK_TRUE(type_string == type_traits::TypeString());
}

TEST(TypeTraits_long_double_tests)
{
    typedef long double type;
    typedef ocl::TypeTraits<type> type_traits;

    std::string type_string = "long double";

    CHECK_TRUE(type_traits::IS_PRIMITIVE);
    CHECK_TRUE(type_traits::IS_SIGNED);

    CHECK_TRUE(type_traits::Zero() == 0.0L);
    CHECK_TRUE(type_traits::One() == 1.0L);
    CHECK_TRUE(type_traits::MinusOne() == -1.0L);
    CHECK_TRUE(type_traits::Min() == DBL_MAX * -1.0L);
    CHECK_TRUE(type_traits::Max() == DBL_MAX);
    CHECK_TRUE(type_traits::Default() == 0.0L);

    CHECK_TRUE(type_string == type_traits::TypeString());
}
