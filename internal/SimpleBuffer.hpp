/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INTERNAL_SIMPLEBUFFER_HPP
#define OCL_GUARD_INTERNAL_SIMPLEBUFFER_HPP

#include "MinMax.hpp"
#include "TypeTraits.hpp"
#include <cstddef>
#include <cstring>
#include <stdexcept>

namespace ocl
{

template<typename Type>
class SimpleBuffer
{
public:
    typedef Type value_type;
    typedef std::size_t size_type;
    typedef TypeTraits<value_type> type_traits;

public:
    SimpleBuffer() noexcept
        : m_buffer(nullptr)
        , m_size(0)
    {
    }

    SimpleBuffer(const SimpleBuffer& buffer)
        : m_buffer(nullptr)
        , m_size(0)
    {
        Copy(buffer);
    }

    SimpleBuffer(SimpleBuffer&& buffer) noexcept
        : m_buffer(buffer.m_buffer)
        , m_size(buffer.m_size)
    {
        buffer.m_buffer = nullptr;
        buffer.m_size = 0U;
    }

    SimpleBuffer(size_type size)
        : m_buffer(new value_type[size])
        , m_size(size)
    {
        UnsafeFill(m_buffer, m_size, type_traits::Default());
    }

    SimpleBuffer(value_type const* ptr, size_type size)
        : m_buffer(new value_type[size])
        , m_size(size)
    {
        if (ptr != nullptr)
            ::memcpy(m_buffer, ptr, m_size * sizeof(value_type));
    }

    ~SimpleBuffer()
    {
        delete m_buffer;
    }

    SimpleBuffer& operator =(SimpleBuffer const& buffer)
    {
        Copy(buffer);
        return *this;
    }

    SimpleBuffer& operator =(SimpleBuffer&& buffer) noexcept
    {
        Move(buffer);
        return *this;
    }

    value_type operator [](size_type index) const
    {
        if (index >= m_size)
            throw std::range_error("Index for buffer out of range");
        return m_buffer[index];
    }

    value_type& operator [](size_type index)
    {
        if (index >= m_size)
            throw std::range_error("Index for buffer out of range");
        return m_buffer[index];
    }

    // Get number of values the buffer can store.
    size_type GetSize() const noexcept
    {
        return m_size;
    }

    // Return first value in buffer or default value when buffer is empty.
    value_type GetFirst() const noexcept
    {
        return m_size > 0U ? m_buffer[0U] : type_traits::Default();
    }

    // Return last value in buffer or default value when buffer is empty.
    value_type GetLast() const noexcept
    {
        return m_size > 0U ? m_buffer[m_size - 1U] : type_traits::Default();
    }

    // Return true when buffer size is zero.
    bool IsEmpty() const noexcept
    {
        return m_size == 0U;
    }

    // Get the raw pointer.
    value_type const* GetPointer() const noexcept
    {
        return m_buffer;
    }

    // Get the raw pointer.
    value_type* GetPointer() noexcept
    {
        return m_buffer;
    }

    // Resize the buffer, and optionally keeping original values by copying to new buffer.
    // can also optionally clear any newly allocated values.
    void Resize(size_type new_size, bool keep_original = true, bool clear_extra = true)
    {
        if (new_size > 0U)
        {
            value_type* new_buffer = new value_type[new_size];
            size_type copy_size = MinMax<size_type>::Min(m_size, new_size);
            if (keep_original && m_size > 0U)
                memcpy(new_buffer, m_buffer, copy_size);
            if (clear_extra)
            {
                if (!keep_original)
                    UnsafeFill(new_buffer, new_size, type_traits::Default());
                else if (new_size > m_size)
                    UnsafeSet(new_buffer, new_size, m_size, new_size - 1, type_traits::Default());

            }
            delete m_buffer;
            m_buffer = new_buffer;
            m_size = new_size;
        }
        else
            Clear();
    }

    // Grow buffer by a specified increased size.
    void Grow(size_type grow_by = 1, bool keep_original = true, bool clear_extra = true)
    {
        Resize(m_size + grow_by, keep_original, clear_extra);
    }

    void Shrink(size_type shrink_by = 1, bool keep_original = true, bool clear_remaining = true)
    {
        size_type new_size = shrink_by < m_size ? m_size - shrink_by : 0U;
        Resize(new_size, keep_original, clear_remaining);
    }

    // Free the memory and set buffer size to zero.
    void Clear()
    {
        delete m_buffer;
        m_buffer = nullptr;
        m_size = 0U;
    }

    // Fill the buffer with a value for entire range.
    void Fill(value_type value) noexcept
    {
        if (m_buffer != nullptr)
            UnsafeFill(m_buffer, m_size, value);
    }

    void SetAt(size_type index, value_type value)
    {
        if (index < m_size)
            m_buffer[index] = value;
        else
            throw std::out_of_range("SetAt : out of range");
    }

    // Set a range of values to a specific value.
    void Set(size_type start = type_traits::Zero(),
             size_type end = type_traits::Max(),
             value_type value = type_traits::Default()) noexcept
    {
        if (start < m_size)
        {
            value_type* buffer_end  = m_buffer + MinMax<size_type>::Min(m_size, end + 1);
            for (value_type* buffer_pos = m_buffer + start; buffer_pos < buffer_end; ++buffer_pos)
                *buffer_pos = value;
        }
    }

    // Copy the source buffer to this buffer.
    void Copy(SimpleBuffer const& buffer)
    {
        if (!buffer.IsEmpty())
        {
            Resize(buffer.GetSize(), false, false);
            ::memcpy(m_buffer, buffer.m_buffer, m_size * sizeof(value_type));
        }
        else
            Clear();
    }

    // Copy the values from pointer to this buffer.
    void Copy(value_type const* values, size_type count)
    {
        if (values != nullptr && count > 0U)
        {
            Resize(count, false, false);
            ::memcpy(m_buffer, values, count * sizeof(value_type));
        }
        else
            Clear();
    }

    // Move the source buffer to this buffer.
    void Move(SimpleBuffer& buffer) noexcept
    {
        delete m_buffer;
        m_buffer = buffer.m_buffer;
        m_size = buffer.m_size;
        buffer.m_buffer = nullptr;
        buffer.m_size = 0U;
    }

    // Swap two buffers.
    void Swap(SimpleBuffer<value_type>& other) noexcept
    {
        value_type* tmp_buffer = other.m_buffer;
        size_type tmp_size = other.m_size;
        other.m_buffer = m_buffer;
        other.m_size = m_size;
        m_buffer = tmp_buffer;
        m_size = tmp_size;
    }

private:
    // Unsafe function to fill the buffer with a value for entire range.
    static void UnsafeFill(value_type* buffer, size_type size, value_type value) noexcept
    {
        value_type* curr = buffer;
        value_type* buffer_end = curr + size;
        for (; curr < buffer_end; ++curr)
            *curr = value;
    }

    static void UnsafeSet(value_type* buffer,
                          size_type size,
                          size_type start = type_traits::Zero(),
                          size_type end = type_traits::Max(),
                          value_type value = type_traits::Default()) noexcept
    {
        value_type* buffer_end = buffer + MinMax<size_type>::Min(size, end + 1);
        for (value_type* buffer_pos = buffer + start; buffer_pos < buffer_end; ++buffer_pos)
            *buffer_pos = value;
    }

private:
    value_type* m_buffer;
    size_type   m_size;
};

}

#endif // OCL_GUARD_INTERNAL_SIMPLEBUFFER_HPP
