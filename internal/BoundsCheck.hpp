/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INTERNAL_BOUNDSCHECK_HPP
#define OCL_GUARD_INTERNAL_BOUNDSCHECK_HPP

#include "TypeTraits.hpp"
#include "SignedBoundsCheck.hpp"
#include "UnsignedBoundsCheck.hpp"

namespace ocl
{

template<typename Type>
class BoundsCheck
{
public:
    typedef Type type;
    typedef TypeTraits<type> type_traits;
    typedef typename type_traits::signed_type signed_type;

    static bool CanAdd(type a, type b) noexcept
    {
        return type_traits::IS_SIGNED ? SignedBoundsCheck<type>::CanAdd(a, b)
                                      : UnignedBoundsCheck<type>::CanAdd(a, b);
    }

    static bool CanSubtract(type a, type b) noexcept
    {
        return type_traits::IS_SIGNED ? SignedBoundsCheck<type>::CanSubtract(a, b)
                                      : UnignedBoundsCheck<type>::CanSubtract(a, b);
    }
};

} // namespace ocl

#endif // OCL_GUARD_INTERNAL_BOUNDSCHECK_HPP
