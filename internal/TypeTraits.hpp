/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INTERNAL_TYPETRAITS_TYPES_HPP
#define OCL_GUARD_INTERNAL_TYPETRAITS_TYPES_HPP

#include <climits>
#include <stdexcept>


#ifdef MIN
#error Cannot use TypeTraits.hpp when MIN is defined.
#endif

#ifdef MAX
#error Cannot use TypeTraits.hpp when MAX is defined.
#endif

namespace ocl
{

template<typename Type>
struct TypeTraits
{
    typedef Type type;

    static constexpr bool IS_PRIMITIVE = false;

    static constexpr type Default() noexcept { return type(); }

    static char const* TypeString() noexcept { return ""; }
};

template<>
struct TypeTraits<char>
{
    // Type defines.
    typedef char type;
    typedef signed char signed_type;
    typedef unsigned char unsigned_type;

    // Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;
    static constexpr type ZERO         = 0;
    static constexpr type ONE          = 1;
    static constexpr type MINUS_ONE    = static_cast<char>(-1);
    static constexpr type MIN          = SCHAR_MIN;
    static constexpr type MAX          = SCHAR_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = static_cast<type>(UCHAR_MAX);
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }
    static constexpr type TopBit() noexcept   { return TOP_BIT; }
    static constexpr type Default() noexcept  { return ZERO; }

    static char const* TypeString() noexcept  { return "char"; }

    static bool IsNegative(type value) noexcept
    {
        return value < ZERO;
    }

    // Return the absolute (i.e. positive) value or MIN if value is MIN.
    static type Abs(type value) noexcept
    {
        return (value < ZERO) && (value != MIN) ? -value : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < ZERO ? b < ZERO : b >= ZERO;
    }
};

template<>
struct TypeTraits<signed char>
{
// Type defines.
    typedef signed char type;
    typedef type signed_type;
    typedef unsigned char unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;
    static constexpr type ZERO         = 0;
    static constexpr type ONE          = 1;
    static constexpr type MINUS_ONE    = static_cast<signed char>(-1);
    static constexpr type MIN          = SCHAR_MIN;
    static constexpr type MAX          = SCHAR_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = static_cast<type>(UCHAR_MAX);
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }
    static constexpr type TopBit() noexcept   { return TOP_BIT; }
    static constexpr type Default() noexcept  { return ZERO; }

    static char const* TypeString() noexcept { return "signed char"; }

    static bool IsNegative(type value) noexcept
    {
        return value < ZERO;
    }

    // Return the absolute (i.e. positive) value or MIN if value is MIN.
    static type Abs(type value) noexcept
    {
        return (value < ZERO) && (value != MIN) ? -value : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < ZERO ? b < ZERO : b >= ZERO;
    }
};

template<>
struct TypeTraits<unsigned char>
{
// Type defines.
    typedef unsigned char type;
    typedef signed char signed_type;
    typedef type unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = false;
    static constexpr type ZERO         = 0;
    static constexpr type ONE          = 1;
    static constexpr type MIN          = 0U;
    static constexpr type MAX          = UCHAR_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = MAX;
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept    { return ZERO; }
    static constexpr type One() noexcept     { return ONE; }
    static constexpr type Min() noexcept     { return MIN; }
    static constexpr type Max() noexcept     { return MAX; }
    static constexpr type TopBit() noexcept  { return TOP_BIT; }
    static constexpr type Default() noexcept { return ZERO; }

    static char const* TypeString() noexcept { return "unsigned char"; }

    static bool IsNegative(type) noexcept
    {
        return false;
    }

    static type Abs(type value) noexcept
    {
        return value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type, type) noexcept
    {
        return true;
    }
};

template<>
struct TypeTraits<signed short>
{
// Type defines.
    typedef signed short type;
    typedef type signed_type;
    typedef unsigned short unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;
    static constexpr type ZERO         = 0;
    static constexpr type ONE          = 1;
    static constexpr type MINUS_ONE    = -1;
    static constexpr type MIN          = SHRT_MIN;
    static constexpr type MAX          = SHRT_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = static_cast<type>(USHRT_MAX);
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }
    static constexpr type TopBit() noexcept   { return TOP_BIT; }
    static constexpr type Default() noexcept  { return ZERO; }

    static char const* TypeString() noexcept { return "signed short"; }

    static bool IsNegative(type value) noexcept
    {
        return value < ZERO;
    }

    // Return the absolute (i.e. positive) value or MIN if value is MIN.
    static type Abs(type value) noexcept
    {
        return (value < ZERO) && (value != MIN) ? -value : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < ZERO ? b < ZERO : b >= ZERO;
    }
};

template<>
struct TypeTraits<unsigned short>
{
// Type defines.
    typedef unsigned short type;
    typedef signed short signed_type;
    typedef type unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = false;
    static constexpr type ZERO         = 0U;
    static constexpr type ONE          = 1U;
    static constexpr type MIN          = 0U;
    static constexpr type MAX          = USHRT_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = MAX;
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept    { return ZERO; }
    static constexpr type One() noexcept     { return ONE; }
    static constexpr type Min() noexcept     { return MIN; }
    static constexpr type Max() noexcept     { return MAX; }
    static constexpr type TopBit() noexcept  { return TOP_BIT; }
    static constexpr type Default() noexcept { return ZERO; }

    static char const* TypeString() noexcept { return "unsigned short"; }

    static bool IsNegative(type) noexcept
    {
        return false;
    }

    static type Abs(type value) noexcept
    {
        return value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type, type) noexcept
    {
        return true;
    }
};

template<>
struct TypeTraits<signed int>
{
// Type defines.
    typedef signed int type;
    typedef type signed_type;
    typedef unsigned int unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;
    static constexpr type ZERO         = 0;
    static constexpr type ONE          = 1;
    static constexpr type MINUS_ONE    = -1;
    static constexpr type MIN          = INT_MIN;
    static constexpr type MAX          = INT_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = static_cast<type>(UINT_MAX);
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }
    static constexpr type TopBit() noexcept   { return TOP_BIT; }
    static constexpr type Default() noexcept  { return ZERO; }

    static char const* TypeString() noexcept { return "signed int"; }

    static bool IsNegative(type value) noexcept
    {
        return value < ZERO;
    }

    // Return the absolute (i.e. positive) value or MIN if value is MIN.
    static type Abs(type value) noexcept
    {
        return (value < ZERO) && (value != MIN) ? -value : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < ZERO ? b < ZERO : b >= ZERO;
    }
};

template<>
struct TypeTraits<unsigned int>
{
// Type defines.
    typedef unsigned int type;
    typedef signed int signed_type;
    typedef type unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = false;
    static constexpr type ZERO         = 0U;
    static constexpr type ONE          = 1U;
    static constexpr type MIN          = 0U;
    static constexpr type MAX          = UINT_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = MAX;
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept    { return ZERO; }
    static constexpr type One() noexcept     { return ONE; }
    static constexpr type Min() noexcept     { return MIN; }
    static constexpr type Max() noexcept     { return MAX; }
    static constexpr type TopBit() noexcept  { return TOP_BIT; }
    static constexpr type Default() noexcept { return ZERO; }

    static char const* TypeString() noexcept { return "unsigned int"; }

    static bool IsNegative(type) noexcept
    {
        return false;
    }

    static type Abs(type value) noexcept
    {
        return value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type, type) noexcept
    {
        return true;
    }
};

template<>
struct TypeTraits<signed long>
{
// Type defines.
    typedef signed long type;
    typedef type signed_type;
    typedef unsigned long unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;
    static constexpr type ZERO         = 0L;
    static constexpr type ONE          = 1L;
    static constexpr type MINUS_ONE    = -1L;
    static constexpr type MIN          = LONG_MIN;
    static constexpr type MAX          = LONG_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = static_cast<type>(ULONG_MAX);
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }
    static constexpr type TopBit() noexcept   { return TOP_BIT; }
    static constexpr type Default() noexcept  { return ZERO; }

    static char const* TypeString() noexcept { return "signed long"; }

    static bool IsNegative(type value) noexcept
    {
        return value < ZERO;
    }

    // Return the absolute (i.e. positive) value or MIN if value is MIN.
    static type Abs(type value) noexcept
    {
        return (value < ZERO) && (value != MIN) ? -value : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < ZERO ? b < ZERO : b >= ZERO;
    }
};

template<>
struct TypeTraits<unsigned long>
{
// Type defines.
    typedef unsigned long type;
    typedef signed long signed_type;
    typedef type unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = false;
    static constexpr type ZERO         = 0UL;
    static constexpr type ONE          = 1UL;
    static constexpr type MIN          = 0UL;
    static constexpr type MAX          = ULONG_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = MAX;
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept    { return ZERO; }
    static constexpr type One() noexcept     { return ONE; }
    static constexpr type Min() noexcept     { return MIN; }
    static constexpr type Max() noexcept     { return MAX; }
    static constexpr type TopBit() noexcept  { return TOP_BIT; }
    static constexpr type Default() noexcept { return ZERO; }

    static char const* TypeString() noexcept { return "unsigned long"; }

    static bool IsNegative(type) noexcept
    {
        return false;
    }

    static type Abs(type value) noexcept
    {
        return value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type, type) noexcept
    {
        return true;
    }
};

template<>
struct TypeTraits<signed long long>
{
// Type defines.
    typedef signed long long type;
    typedef type signed_type;
    typedef unsigned long long unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;
    static constexpr type ZERO         = 0LL;
    static constexpr type ONE          = 1LL;
    static constexpr type MINUS_ONE    = -1LL;
    static constexpr type MIN          = LLONG_MIN;
    static constexpr type MAX          = LLONG_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = static_cast<type>(ULLONG_MAX);
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept     { return ZERO; }
    static constexpr type One() noexcept      { return ONE; }
    static constexpr type MinusOne() noexcept { return MINUS_ONE; }
    static constexpr type Min() noexcept      { return MIN; }
    static constexpr type Max() noexcept      { return MAX; }
    static constexpr type Default() noexcept  { return ZERO; }

    static char const* TypeString() noexcept { return "signed long long"; }

    static bool IsNegative(type value) noexcept
    {
        return value < ZERO;
    }

    // Return the absolute (i.e. positive) value or MIN if value is MIN.
    static type Abs(type value) noexcept
    {
        return (value < ZERO) && (value != MIN) ? -value : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < ZERO ? b < ZERO : b >= ZERO;
    }
};

template<>
struct TypeTraits<unsigned long long>
{
// Type defines.
    typedef unsigned long long type;
    typedef signed long long signed_type;
    typedef type unsigned_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = false;
    static constexpr type ZERO         = 0ULL;
    static constexpr type ONE          = 1ULL;
    static constexpr type MIN          = 0ULL;
    static constexpr type MAX          = ULLONG_MAX;
    static constexpr type TOP_BIT      = MAX - (MAX >> ONE);
    static constexpr type ALL_BITS     = MAX;
    static constexpr type DEFAULT      = ZERO;

    static constexpr type Zero() noexcept    { return ZERO; }
    static constexpr type One() noexcept     { return ONE; }
    static constexpr type Min() noexcept     { return MIN; }
    static constexpr type Max() noexcept     { return MAX; }
    static constexpr type TopBit() noexcept  { return TOP_BIT; }
    static constexpr type Default() noexcept { return ZERO; }

    static char const* TypeString() noexcept { return "unsigned long long"; }

    static bool IsNegative(type) noexcept
    {
        return false;
    }

    static type Abs(type value) noexcept
    {
        return value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type, type) noexcept
    {
        return true;
    }
};

template<>
struct TypeTraits<float>
{
// Type defines.
    typedef float type;
    typedef type signed_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;

    static constexpr type Zero() noexcept     { return 0.0f; }
    static constexpr type One() noexcept      { return 1.0f; }
    static constexpr type MinusOne() noexcept { return -1.0f; }
    static constexpr type Min() noexcept      { return FLT_MAX * -1.0f; }
    static constexpr type Max() noexcept      { return FLT_MAX; }
    static constexpr type Default() noexcept  { return Zero(); }

    static char const* TypeString() noexcept { return "float"; }

    static bool IsNegative(type value) noexcept
    {
        return value < Zero();
    }

    static type Abs(type value) noexcept
    {
        return value < Zero() ? value * MinusOne() : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < Zero() ? b < Zero() : !(b < Zero());
    }
};

template<>
struct TypeTraits<double>
{
// Type defines.
    typedef double type;
    typedef type signed_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;

    static constexpr type Zero() noexcept     { return 0.0; }
    static constexpr type One() noexcept      { return 1.0; }
    static constexpr type MinusOne() noexcept { return -1.0; }
    static constexpr type Min() noexcept      { return DBL_MAX * -1.0; }
    static constexpr type Max() noexcept      { return DBL_MAX; }
    static constexpr type Default() noexcept  { return Zero(); }

    static char const* TypeString() noexcept { return "double"; }

    static bool IsNegative(type value) noexcept
    {
        return value < Zero();
    }

    static type Abs(type value) noexcept
    {
        return value < Zero() ? value * MinusOne() : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < Zero() ? b < Zero() : !(b < Zero());
    }
};

template<>
struct TypeTraits<long double>
{
// Type defines.
    typedef long double type;
    typedef type signed_type;

// Constants.
    static constexpr bool IS_PRIMITIVE = true;
    static constexpr bool IS_SIGNED    = true;

    static constexpr type Zero() noexcept     { return 0.0L; }
    static constexpr type One() noexcept      { return 1.0L; }
    static constexpr type MinusOne() noexcept { return -1.0L; }
    static constexpr type Min() noexcept      { return LDBL_MAX * -1.0L; }
    static constexpr type Max() noexcept      { return LDBL_MAX; }
    static constexpr type Default() noexcept  { return Zero(); }

    static char const* TypeString() noexcept { return "long double"; }

    static bool IsNegative(type value) noexcept
    {
        return value < Zero();
    }

    static type Abs(type value) noexcept
    {
        return value < Zero() ? value * MinusOne() : value;
    }

    // Return true if both values are negative or both values are positive.
    static bool SameSign(type a, type b) noexcept
    {
        return a < Zero() ? b < Zero() : !(b < Zero());
    }
};

} // namespace ocl

#endif // OCL_GUARD_INTERNAL_TYPETRAITS_TYPES_HPP
