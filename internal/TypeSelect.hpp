/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPES_INTERNAL_TYPESELECT_HPP
#define OCL_GUARD_TYPES_INTERNAL_TYPESELECT_HPP

namespace ocl
{

template<typename FirstType, typename SecondType, bool const first>
struct TypeSelect;

template<typename FirstType, typename SecondType>
struct TypeSelect<FirstType, SecondType, true>
{
    typedef FirstType  first_type;
    typedef SecondType second_type;
    typedef FirstType  selected_type;
    typedef SecondType other_type;
};

template<typename FirstType, typename SecondType>
struct TypeSelect<FirstType, SecondType, false>
{
    typedef FirstType  first_type;
    typedef SecondType second_type;
    typedef SecondType selected_type;
    typedef FirstType  other_type;
};

} // namespace ocl

#endif // OCL_GUARD_TYPES_INTERNAL_TYPESELECT_HPP
