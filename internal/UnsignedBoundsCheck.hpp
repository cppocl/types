/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INTERNAL_UNSIGNEDBOUNDSCHECK_HPP
#define OCL_GUARD_INTERNAL_UNSIGNEDBOUNDSCHECK_HPP

#include "TypeTraits.hpp"

namespace ocl
{

template<typename Type>
class UnignedBoundsCheck
{
public:
    typedef Type type;
    typedef TypeTraits<type> type_traits;
    typedef typename type_traits::signed_type signed_type;

    static bool CanAdd(type a, type b) noexcept
    {
        return b <= type_traits::Max() - a;
    }

    static bool CanSubtract(type a, type b) noexcept
    {
        return b <= a;
    }
};

} // namespace ocl

#endif // OCL_GUARD_INTERNAL_UNSIGNEDBOUNDSCHECK_HPP
