/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_INTERNAL_SIGNEDBOUNDSCHECK_HPP
#define OCL_GUARD_INTERNAL_SIGNEDBOUNDSCHECK_HPP

#include "TypeTraits.hpp"

namespace ocl
{

template<typename Type>
class SignedBoundsCheck
{
public:
    typedef Type type;
    typedef TypeTraits<type> type_traits;
    typedef typename type_traits::signed_type signed_type;

    static bool CanAdd(type a, type b) noexcept
    {
        // When one value is positive and one value is negative,
        // there will never be an overflow or underflow.
        // Otherwise test range for negative and positive separately.

        return !type_traits::SameSign(a, b) ||
            (type_traits::IsNegative(a) ? CanAddNegatives(a, b) : CanAddPositives(a, b));
    }

    static bool CanSubtract(type a, type b) noexcept
    {
        return type_traits::SameSign(a, b) ||
            (type_traits::IsNegative(a) ? CanSubtractPositiveFromNegative(a, b)
                : CanSubtractNegativeFromPositive(a, b));
    }

private:
    // Handle underflow condition of two negative values.
    static bool CanAddNegatives(type a, type b) noexcept
    {
        return b >= type_traits::Min() - a;
    }

    // Handle overflow condition of two positive values.
    static bool CanAddPositives(type a, type b) noexcept
    {
        return b <= type_traits::Max() - a;
    }

    // Check when subtracting the positive number from the negative number there is no underflow.
    static bool CanSubtractPositiveFromNegative(type negative, type positive) noexcept
    {
        return negative >= type_traits::Min() - (-positive);
    }

    // Check when subtracting the negative number from the positive number there is no overflow.
    static bool CanSubtractNegativeFromPositive(type positive, type negative) noexcept
    {
        return (negative != type_traits::Min()) &&
            (type_traits::Abs(negative) <= type_traits::Max() - positive);
    }
};

} // namespace ocl

#endif // OCL_GUARD_INTERNAL_SIGNEDBOUNDSCHECK_HPP
