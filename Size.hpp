/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPES_SIZE_HPP
#define OCL_GUARD_TYPES_SIZE_HPP

#include "internal/BoundsCheck.hpp"
#include "internal/TypeTraits.hpp"

namespace ocl
{

template<typename Type>
class Size
{
public:
    typedef Type type;
    typedef TypeTraits<type> type_traits;
    typedef typename type_traits::signed_type signed_type;
    typedef typename type_traits::unsigned_type unsigned_type;
    typedef Size<signed_type> signed_size_type;
    typedef Size<unsigned_type> unsigned_size_type;

public:
    Size(type width = type_traits::Default(), type height = type_traits::Default()) noexcept
        : m_width(width)
        , m_height(height)
    {
    }

    Size(Size<type> const& size) noexcept
    {
        Copy(size);
    }

    Size(Size<type>&& size) noexcept
    {
        Move(size);
    }

    bool operator ==(Size<type> const& size) const noexcept
    {
        return m_width == size.m_width && m_height == size.m_height;
    }

    bool operator !=(Size<type> const& size) const noexcept
    {
        return m_width != size.m_width || m_height != size.m_height;
    }

    Size<type>& operator =(Size<type> const& size) noexcept
    {
        Copy(size);
        return *this;
    }

    Size<type>& operator +=(Size<type> const& size) noexcept
    {
        m_width += size.m_width;
        m_height += size.m_height;
        return *this;
    }

    Size<type>& operator -=(Size<type> const& size) noexcept
    {
        m_width -= size.m_width;
        m_height -= size.m_height;
        return *this;
    }

    Size<type>& operator *=(Size<type> const& size) noexcept
    {
        m_width *= size.m_width;
        m_height *= size.m_height;
        return *this;
    }

    Size<type>& operator /=(Size<type> const& size) noexcept
    {
        m_width /= size.m_width;
        m_height /= size.m_height;
        return *this;
    }

    Size<type> operator +(Size<type> const& rhs) const noexcept
    {
        Size<type> sz(m_width + rhs.m_width, m_height + rhs.m_height);
        return sz;
    }

    Size<type> operator -(Size<type> const& rhs) const noexcept
    {
        Size<type> sz(m_width - rhs.m_width, m_height - rhs.m_height);
        return sz;
    }

    Size<type> operator *(Size<type> const& rhs) const noexcept
    {
        Size<type> sz(m_width * rhs.m_width, m_height * rhs.m_height);
        return sz;
    }

    Size<type> operator /(Size<type> const& rhs) const noexcept
    {
        Size<type> sz(m_width / rhs.m_width, m_height / rhs.m_height);
        return sz;
    }

    void Copy(Size<type> const& size) noexcept
    {
        m_width = size.m_width;
        m_height = size.m_height;
    }

    void Move(Size<type>& size) noexcept
    {
        m_width = size.m_width;
        m_height = size.m_height;
    }

    type Width() const noexcept
    {
        return m_width;
    }

    type Height() const noexcept
    {
        return m_height;
    }

    void IncrementWidth(type offset) noexcept
    {
        m_width += offset;
    }

    void IncrementHeight(type offset) noexcept
    {
        m_height += offset;
    }

    void IncrementWidthHeight(type offset_width, type offset_height) noexcept
    {
        m_width += offset_width;
        m_height += offset_height;
    }

    void DecrementWidth(type offset) noexcept
    {
        m_width -= offset;
    }

    void DecrementHeight(type offset) noexcept
    {
        m_height -= offset;
    }

    void DecrementWidthHeight(type offset_width, type offset_height) noexcept
    {
        m_width -= offset_width;
        m_height -= offset_height;
    }

    void SetWidth(type width) noexcept
    {
        m_width = width;
    }

    void SetHeight(type height) noexcept
    {
        m_height = height;
    }

    // Set Width and Height to the new values.
    void SetWidthHeight(type width, type height) noexcept
    {
        m_width = width;
        m_height = height;
    }

    // Cast the result of adding Width and offset to 'type' and update Width with new value.
    void OffsetWidth(signed_type offset) noexcept
    {
        m_width = static_cast<type>(m_width + offset);
    }

    // Cast the result of adding Height and offset to 'type' and update Height with new value.
    void OffsetHeight(signed_type offset) noexcept
    {
        m_height = static_cast<type>(m_height + offset);
    }

    // Cast the result of adding Height and offset to 'type' and update Height with new value.
    void OffsetWidthHeight(signed_type offset_width, signed_type offset_height) noexcept
    {
        m_width = static_cast<type>(m_width + offset_width);
        m_height = static_cast<type>(m_height + offset_height);
    }

    // Reset the values to default values, which will be the same as TypeTraits<type>::Default().
    void Clear() noexcept
    {
        m_width = type_traits::Default();
        m_height = type_traits::Default();
    }

    // Check if adding offset_width to Width will cause an overflow or underflow.
    bool BoundsCheckWidth(type offset_width, bool add = true) noexcept
    {
        return add ? BoundsCheck<type>::CanAdd(m_width, offset_width)
                   : BoundsCheck<type>::CanSubtract(m_width, offset_width);
    }

    // Check if adding offset_height to Height will cause an overflow or underflow.
    bool BoundsCheckHeight(type offset_height, bool add = true) noexcept
    {
        return add ? BoundsCheck<type>::CanAdd(m_height, offset_height)
                   : BoundsCheck<type>::CanSubtract(m_height, offset_height);
    }

    // Check if adding offset_width to Width and offset_height to Height will cause an overflow or underflow.
    bool BoundsCheckWidthHeight(type offset_width, type offset_height, bool add = true) noexcept
    {
        return BoundsCheckWidth(offset_width, add) && BoundsCheckHeight(offset_height, add);
    }

    /// Translate this Size from type to signed_type without the need to manually cast Width and Height.
    signed_size_type SignedCast() const noexcept
    {
        return signed_size_type(static_cast<signed_type>(Width()), static_cast<signed_type>(Height()));
    }

    /// Translate the Size from type to signed_type without the need to manually cast Width and Height.
    static signed_size_type SignedCast(Size<type> const& size) noexcept
    {
        return size.SignedCast();
    }

    /// Translate this Size from type to unsigned_type without the need to manually cast Width and Height.
    unsigned_size_type UnsignedCast() const noexcept
    {
        return unsigned_size_type(static_cast<unsigned_type>(Width()), static_cast<unsigned_type>(Height()));
    }

    /// Translate the Size from type to signed_type without the need to manually cast Width and Height.
    static unsigned_size_type UnsignedCast(Size<type> const& size) noexcept
    {
        return size.UnsignedCast();
    }

    /// Get the difference between this size and the other size provided.
    signed_size_type GetDiff(Size<type> const& other) const noexcept
    {
        signed_size_type this_sz(SignedCast());
        Size<signed_type> other_sz(other.SignedCast());
        return signed_size_type(this_sz.Width() - other_sz.Width(), this_sz.Height() - other_sz.Height());
    }

    /// Get the difference between this size and the other size provided.
    Size<type> GetAbsDiff(Size<type> const& other) const noexcept
    {
        signed_size_type this_sz(SignedCast());
        signed_size_type other_sz(other.SignedCast());
        signed_type diff_width  = TypeTraits<signed_type>::Abs(this_sz.Width() - other_sz.Width());
        signed_type diff_height = TypeTraits<signed_type>::Abs(this_sz.Height() - other_sz.Height());
        return Size<type>(static_cast<type>(diff_width), static_cast<type>(diff_height));
    }

    void Swap(Size<type>& other) noexcept
    {
        type width = other.Width();
        type height = other.Height();
        other.SetWidthHeight(Width(), Height());
        SetWidthHeight(width, height);
    }

    // Get the type as a string.
    static constexpr char const* TypeString() noexcept
    {
        return type_traits::TypeString();
    }

private:
    type m_width;
    type m_height;
};

} // namespace ocl

#endif // OCL_GUARD_TYPES_SIZE_HPP
