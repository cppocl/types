/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPES_TRISTATE_HPP
#define OCL_GUARD_TYPES_TRISTATE_HPP

#include <exception>
#include <cassert>

namespace ocl
{

class TriState
{
public:
    enum class State : int
    {
        Unknown = -1,
        False = 0,
        True = 1
    };

public:
    /// Set the default state to Unknown at construction.
    TriState() noexcept
        : m_state(State::Unknown)
    {
    }

    /// Set the state to True or False at construction.
    explicit TriState(bool state) noexcept
        : m_state(state ? State::True : State::False)
    {
    }

    /// Set the state to True, False or Unknown at construction.
    explicit TriState(State state) noexcept
        : m_state(state)
    {
    }

    /// Copy other state at construction.
    explicit TriState(TriState const& state) noexcept
        : m_state(state.m_state)
    {
    }

    /// Move other state at construction, setting other state to Unknown after move.
    explicit TriState(TriState&& state) noexcept
        : m_state(state.m_state)
    {
        state.m_state = State::Unknown;
    }

    /// Copy other state.
    TriState& operator =(TriState const& state) noexcept
    {
        m_state = state.m_state;
        return *this;
    }

    /// Move other state, setting other state to Unknown after move.
    TriState& operator =(TriState&& state) noexcept
    {
        m_state = state.m_state;
        state.m_state = State::Unknown;
        return *this;
    }

    /// Set the state to true or false.
    TriState& operator =(State state) noexcept
    {
        m_state = state;
        return *this;
    }

    /// Set the state to true or false.
    TriState& operator =(bool state) noexcept
    {
        m_state = state ? State::True : State::False;
        return *this;
    }

    /// State can be used as a secure boolean,
    /// which must be set to True or False before being tested.
    operator bool() const
    {
        // NOTE: breakpoint can be placed on the throw line for debugging purposes.
        if (m_state == State::Unknown)
            throw MessageException("TriState is Unknown for operator bool()");

        return m_state == State::True;
    }

    /// Compare two states and return true if they are the same value.
    bool operator ==(TriState const& state) const noexcept
    {
        return m_state == state.m_state;
    }

    /// Compare two states and return true if they are the same value.
    bool operator ==(State const& state) const noexcept
    {
        return m_state == state;
    }

    /// Compare two states and return true if they are the same value.
    bool operator ==(bool state) const
    {
        // NOTE: breakpoint can be placed on the throw line for debugging purposes.
        if (m_state == State::Unknown)
            throw MessageException("TriState is Unknown for operator ==(bool)");

        State st = state ? State::True : State::False;
        return m_state == st;
    }

    /// Check if the state is True or False.
    bool IsKnown() const noexcept
    {
        return m_state != State::Unknown;
    }

    /// Check if the state is Unknown.
    bool IsUnknown() const noexcept
    {
        return m_state == State::Unknown;
    }

    /// Check if the state is True.
    bool IsTrue() const noexcept
    {
        return m_state == State::True;
    }

    /// Check if the state is False.
    bool IsFalse() const noexcept
    {
        return m_state == State::False;
    }

    /// Reset the state to Unknown.
    void Clear() noexcept
    {
        m_state = State::Unknown;
    }

public:
    struct MessageException : public std::exception
    {
        MessageException(char const* msg) : m_msg(msg) {}
        char const* what() const override { return m_msg; }
        private:
            char const* m_msg;
    };

private:
    State m_state;
};

} // namespace ocl

#endif // OCL_GUARD_TYPES_TRISTATE_HPP
