/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TYPES_POINT_HPP
#define OCL_GUARD_TYPES_POINT_HPP

#include "internal/BoundsCheck.hpp"
#include "internal/TypeTraits.hpp"

namespace ocl
{

template<typename Type>
class Point
{
public:
    typedef Type type;
    typedef TypeTraits<type> type_traits;
    typedef typename type_traits::signed_type signed_type;
    typedef typename type_traits::unsigned_type unsigned_type;
    typedef Point<signed_type> signed_point_type;
    typedef Point<unsigned_type> unsigned_point_type;

public:
    Point(type x = type_traits::Default(), type y = type_traits::Default()) noexcept
        : m_x(x)
        , m_y(y)
    {
    }

    Point(Point<type> const& point) noexcept
    {
        Copy(point);
    }

    Point(Point<type>&& point) noexcept
    {
        Move(point);
    }

    bool operator ==(Point<type> const& point) const noexcept
    {
        return m_x == point.m_x && m_y == point.m_y;
    }

    bool operator !=(Point<type> const& point) const noexcept
    {
        return m_x != point.m_x || m_y != point.m_y;
    }

    Point<type>& operator =(Point<type> const& point) noexcept
    {
        Copy(point);
        return *this;
    }

    Point<type>& operator +=(Point<type> const& point) noexcept
    {
        m_x += point.m_x;
        m_y += point.m_y;
        return *this;
    }

    Point<type>& operator -=(Point<type> const& point) noexcept
    {
        m_x -= point.m_x;
        m_y -= point.m_y;
        return *this;
    }

    Point<type>& operator *=(Point<type> const& point) noexcept
    {
        m_x *= point.m_x;
        m_y *= point.m_y;
        return *this;
    }

    Point<type>& operator /=(Point<type> const& point) noexcept
    {
        m_x /= point.m_x;
        m_y /= point.m_y;
        return *this;
    }

    Point<type> operator +(Point<type> const& rhs) const noexcept
    {
        Point<type> pt(m_x + rhs.m_x, m_y + rhs.m_y);
        return pt;
    }

    Point<type> operator -(Point<type> const& rhs) const noexcept
    {
        Point<type> pt(m_x - rhs.m_x, m_y - rhs.m_y);
        return pt;
    }

    Point<type> operator *(Point<type> const& rhs) const noexcept
    {
        Point<type> pt(m_x * rhs.m_x, m_y * rhs.m_y);
        return pt;
    }

    Point<type> operator /(Point<type> const& rhs) const noexcept
    {
        Point<type> pt(m_x / rhs.m_x, m_y / rhs.m_y);
        return pt;
    }

    void Copy(Point<type> const& point) noexcept
    {
        m_x = point.m_x;
        m_y = point.m_y;
    }

    void Move(Point<type>& point) noexcept
    {
        m_x = point.m_x;
        m_y = point.m_y;
    }

    type X() const noexcept
    {
        return m_x;
    }

    type Y() const noexcept
    {
        return m_y;
    }

    void IncrementX(type offset) noexcept
    {
        m_x += offset;
    }

    void IncrementY(type offset) noexcept
    {
        m_y += offset;
    }

    void IncrementXY(type offset_x, type offset_y) noexcept
    {
        m_x += offset_x;
        m_y += offset_y;
    }

    void DecrementX(type offset) noexcept
    {
        m_x -= offset;
    }

    void DecrementY(type offset) noexcept
    {
        m_y -= offset;
    }

    void DecrementXY(type offset_x, type offset_y) noexcept
    {
        m_x -= offset_x;
        m_y -= offset_y;
    }

    void SetX(type x) noexcept
    {
        m_x = x;
    }

    void SetY(type y) noexcept
    {
        m_y = y;
    }

    // Set X and Y to the new values.
    void SetXY(type x, type y) noexcept
    {
        m_x = x;
        m_y = y;
    }

    // Cast the result of adding X and offset to 'type' and update X with new value.
    void OffsetX(signed_type offset) noexcept
    {
        m_x = static_cast<type>(m_x + offset);
    }

    // Cast the result of adding Y and offset to 'type' and update Y with new value.
    void OffsetY(signed_type offset) noexcept
    {
        m_y = static_cast<type>(m_y + offset);
    }

    // Cast the result of adding Y and offset to 'type' and update Y with new value.
    void OffsetXY(signed_type offset_x, signed_type offset_y) noexcept
    {
        m_x = static_cast<type>(m_x + offset_x);
        m_y = static_cast<type>(m_y + offset_y);
    }

    // Reset the values to default values, which will be the same as TypeTraits<type>::Default().
    void Clear() noexcept
    {
        m_x = type_traits::Default();
        m_y = type_traits::Default();
    }

    // Check if adding offset_x to X will cause an overflow or underflow.
    bool BoundsCheckX(type offset_x, bool add = true) noexcept
    {
        return add ? BoundsCheck<type>::CanAdd(m_x, offset_x)
                   : BoundsCheck<type>::CanSubtract(m_x, offset_x);
    }

    // Check if adding offset_y to Y will cause an overflow or underflow.
    bool BoundsCheckY(type offset_y, bool add = true) noexcept
    {
        return add ? BoundsCheck<type>::CanAdd(m_y, offset_y)
                   : BoundsCheck<type>::CanSubtract(m_y, offset_y);
    }

    // Check if adding offset_x to X and offset_y to Y will cause an overflow or underflow.
    bool BoundsCheckXY(type offset_x, type offset_y, bool add = true) noexcept
    {
        return BoundsCheckX(offset_x, add) && BoundsCheckY(offset_y, add);
    }

    /// Translate this Point from type to signed_type without the need to manually cast X and Y.
    signed_point_type SignedCast() const noexcept
    {
        return signed_point_type(static_cast<signed_type>(X()), static_cast<signed_type>(Y()));
    }

    /// Translate the Point from type to signed_type without the need to manually cast X and Y.
    static signed_point_type SignedCast(Point<type> const& point) noexcept
    {
        return point.SignedCast();
    }

    /// Translate this Point from type to unsigned_type without the need to manually cast X and Y.
    unsigned_point_type UnsignedCast() const noexcept
    {
        return unsigned_point_type(static_cast<unsigned_type>(X()), static_cast<unsigned_type>(Y()));
    }

    /// Translate the Point from type to signed_type without the need to manually cast X and Y.
    static unsigned_point_type UnsignedCast(Point<type> const& point) noexcept
    {
        return point.UnsignedCast();
    }

    /// Get the difference between this point and the other point provided.
    signed_point_type GetDiff(Point<type> const& other) const noexcept
    {
        signed_point_type this_pt(SignedCast());
        Point<signed_type> other_pt(other.SignedCast());
        return signed_point_type(this_pt.X() - other_pt.X(), this_pt.Y() - other_pt.Y());
    }

    /// Get the difference between this point and the other point provided.
    Point<type> GetAbsDiff(Point<type> const& other) const noexcept
    {
        signed_point_type this_pt(SignedCast());
        signed_point_type other_pt(other.SignedCast());
        signed_type diff_x = TypeTraits<signed_type>::Abs(this_pt.X() - other_pt.X());
        signed_type diff_y = TypeTraits<signed_type>::Abs(this_pt.Y() - other_pt.Y());
        return Point<type>(static_cast<type>(diff_x), static_cast<type>(diff_y));
    }

    void Swap(Point<type>& other) noexcept
    {
        type x = other.X();
        type y = other.Y();
        other.SetXY(X(), Y());
        SetXY(x, y);
    }

    // Get the type as a string.
    static constexpr char const* TypeString() noexcept
    {
        return type_traits::TypeString();
    }

private:
    type m_x;
    type m_y;
};

} // namespace ocl

#endif // OCL_GUARD_TYPES_POINT_HPP
